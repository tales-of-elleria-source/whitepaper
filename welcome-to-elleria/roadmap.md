---
description: >-
  While there are no fixed timelines to accommodate to sudden changes, our
  developers will provide constant updates and announcements on the progress of
  milestones.
---

# Roadmap

![The City of Elleria](../.gitbook/assets/worldmap\_v1.png)

## Phase Alpha

At the starting point of TELL, we will be releasing a browser-based idle yet immersive experience that will allow players to get acquainted with what Elleria is all about.

{% hint style="info" %}
This roadmap is currently outdated as our plans has diverged as a response to community feedback, tokenomics, analytics, and market conditions. Expect this to be updated soon!
{% endhint %}

### Phase 0

~~**0. Release Whitepaper, Twitter, Discord, and Website**~~

~~**0.1 Testnet Launch & Public Testing**~~

~~**0.2 Hero Summoning**~~

~~Presales\*\*:\*\* 21st March 2022~~

~~**0.3 Official Launch: Deployment of Tokens, Release of Assignments (fixed emissions)**~~

~~**0.4 Questing (Solo)**~~

~~Quest system will be implemented. These quests will drop tokens to heroes who participated/completed them successfully.~~

### Phas**e 1**

#### ~~**1.1 Liquidity Mining**~~

~~**1.2 New Assignments (Pooled Emissions)**~~

~~New assignments will be introduced with pool-based emissions.~~

~~**1.3 Equipment System (Cosmetics)**~~

~~An equipment system will be introduced.~~

~~**1.4 Leaderboard Feature**~~

~~Introduction of a leaderboard where top players will be featured and rewarded.~~

### Phas**e 2**

~~**2.1 New Realm - New Classes**~~

~~Elleria exists in a metaverse with multiple realms. As the world expands, Elysis can connect to souls from higher realms, allowing for an introduction of a batch of new classes.~~

~~**2.2 Hero Recycling Feature**~~

~~The souls of heroes can be fused using divinity.~~

~~**2.3 New Quests (Tower of Babel)**~~

~~New quests will be released that involve multiple heroes. More details to be announced.~~

### Phas**e 3**

~~**3.1 Equipment System**~~

~~Introduction of~~ [~~Equipment~~](../gameplay-prologue/game-features/equipment.md)~~. More details to be announced.~~

~~**3.2 Guide Book**~~

~~Introduction of Guidebook. Achieve milestones to unlock rewards for faster progression.~~

~~**3.3 Battle Pass**~~

~~Introduction of~~ [~~Battle Pass~~](../gameplay-prologue/battle-pass.md)~~. Obtain free/paid rewards from complete battle pass missions.~~

### Phas**e 4**

**4.1 Skills Revamp**

Introduction of Skills. Enhance your gameplay by equipping multiple skills to enhance your character.\
\
**4.2 Introduction of Coliseum/ Arena Battles (PVP)**

PVP system will be implemented. Challenge other players to earn rewards and power up your hero!



##

![Warrior test animation](../.gitbook/assets/WarriorWalk.gif)

## **Phase Beta**

As the browser-based roadmap is being continuously rolled out, we are concurrently developing a **Three-Dimensional Racer Mobile Game** for full immersion and proper real-time skill-based gameplay.\\

<figure><img src="../.gitbook/assets/RAcers.png" alt=""><figcaption><p>Beach Map</p></figcaption></figure>

\
Ellerian Racers sneak peek available [here](https://twitter.com/TalesofElleria/status/1579864403729580032).

\\

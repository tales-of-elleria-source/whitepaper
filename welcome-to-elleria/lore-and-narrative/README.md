---
description: >-
  Dualism is the key theme behind Elleria's lore. The world's 'balance' has been
  upset by outside forces, and we take the viewpoint of humans as protagonist,
  seeking to restore peace.
---

# Lore & Narrative

![](<../../.gitbook/assets/ellerium papers.png>)

### Chapter 0: The Cataclysm

Welcome to the world of Elleria, a vast and prosperous singular continent. Resources were abundant throughout Elleria. Mines filled with gold and precious gems; Lush forests filled with tall, verdant trees; Animals roamed the plains and lived in harmony with humans. Citizens lived carefree centuries after centuries.

However, everything changed when a great fire dragon, Ignacio, abruptly flew into a rage and wreaked havoc across the continent. Forests turned into ashes, cities, and castles were devastated and lay in ruins. With no way to resist, screams of desperation filled the air, and chaos ensued. It was pandemonium.

When all hope seemed to be lost, the Goddess of Elleria, Elysis, appeared. She cast divine magic upon Ignacio, gravely wounding him. In pain and distress, Ignacio finally escaped back to its nest, falling into hibernation to rejuvenate its life force.

Unable to take the life of the fire dragon, Elysis instead expended her divinity and bestowed a blessing on all of Elleria’s citizens, and the continent itself, leaving behind a revelation before returning to the realm of gods.

The survivors congregated in a single remaining city to rebuild it and restart life anew. Twenty years later- Monsters have started to appear and are closing in on the city. According to the Goddess’s prophecy, the appearance of monsters is an omen that Ignacio would awaken once again… But this time, the citizens are not helpless.

> _Balance has been broken, the future grows uncertain._\
> _Leave behind your sorrows, let I bear thy burdens._\
> _The blessing on this continent, in future will be pertinent._\
> _Should foul creatures appear, a path will be made clear._\
> _Though the dragon awakens, your fate can be rewritten._

With Ellerium, said to be the crystallized blessing of the Goddess, citizens can summon strong heroes and forge blessed weapons effective against the monsters. Will you help to protect Elleria from the monsters and start your epic adventure to slay the evil dragon before it awakens?

### Chapter 1: Tremors

Despite the prophecy, the citizens started panicking when the monsters first started making their appearances.

_“Is the dragon attacking us again?!”, “Is our city going to be wiped out?”, “ Are we going to die??”_

_“The blessings are nothing more than shiny rocks. How can we defeat THOSE THINGS!?”_

rumble rumble

Right before the city was about to fall into turmoil from fear of the monsters, tremors swept across the land, shaking and knocking people off their feet.\
\
"_Were everyone’s lives coming to an end?"_\
_"Is the rumbling a sign of the dragon’s awakening?"_\
_"Was their Goddess’ blessing powerless after all?"_

“Look!!!” someone shouted at the top of their lungs amidst the voice of the roaring Earth. A dark shadow was cast over the city. Those who heard his cries clumsily grasped onto supports, getting themselves barely stable enough to allow them a glimpse of their surroundings, holding their breaths for what seemed to be an inevitable end. Despite being mentally prepared for the sight of a dragon descending upon them, what they saw was completely out of their expectations- A cliff now towered over their city, its colossal form blocking out the sun, jutting out over the coast.

…

A moment of silence ensued until everyone suddenly snapped out of their stupor. Almost as though an invisible string pulled them, everyone felt an instinctive urge to head towards the top of the cliff and slowly made their way towards the peak. Amongst the shuffling of footsteps, those observant enough would have realised that the mined Ellerium displayed around town were glowing- almost as though they were reacting to a divine presence.

…

As the citizens approached the peak, a platform carved entirely out of stone showed itself, with engravings of what seemed like divine runes, glowing and pulsating. A messy cut path led from where they stood, leading into the middle of the stone-looking device, where what looked like a colossal mass of Ellerium was hovering and glowing.

Ahead of the glowing crystal stood a tablet. Amongst everyone’s curiosity and fear, a brave young lad went forward. He discovered that there were writings in their common tongue engraved unto the tablet:

Summoning Altar:

> _Crystallised divinity can lure over souls from a higher realm._\
> _Speak with my apostle, o’ loyal believer;_\
> _A pact to your soul, offer them physicality;_\
> _Otherwise souls they might be, wandering for eternity._

With this, the citizens realised they could summon the souls of fallen heroes through the altar by offering Ellerium. Speaking to the apostle would allow someone to bind a soul to them, giving the soul a physical form- a second chance to live, train, and grow stronger within Elleria, in exchange for their loyalty.

### Chapter 2: Souls & Higher Realms

\--

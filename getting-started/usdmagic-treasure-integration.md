# $MAGIC / Treasure Integration

As huge fans of TreasureDAO and the framework that they are building, we are delighted to be given the opportunity to integrate into their ecosystem.

## Treasure Marketplace Listing

Tales of Elleria will be listed on the Trove Marketplace!\
Heroes minted during the whitelist and public mint will be available to trade using $MAGIC.

Refer to TreasureDAO’s whitepaper on how to use the Treasure Marketplace:\
[https://docs.treasure.lol/marketplaces/using-the-marketplace](https://docs.treasure.lol/marketplaces/using-the-marketplace)

## Treasury Swap <a href="#e2ab" id="e2ab"></a>

To better align the long-term interests of TreasureDAO and Tales of Elleria, we will be providing TreasureDAO with a commensurate value of NFTs & native tokens in return for $MAGIC grants.

Tales of Elleria will exchange 5% of all mintable NFTs, $MEDALS and $ELM in exchange of a $MAGIC grant emission schedule. This $MAGIC grant will allow players to earn $MAGIC from playing Tales of Elleria.\
\
More updates on how to earn $MAGIC coming soon...

## Bootstrapped Liquidity and Funding <a href="#918e" id="918e"></a>

TreasureDAO will help Tales of Elleria bootstrap the project with capital in its initial stages. Tales of Elleria’s native tokens ($MEDALS & $ELM) will be paired with $MAGIC in the primary liquidity pool. The Liquidity pool seeded by TreasureDAO will also be launching on Treasure’s AMM (if and when one is developed).

## Content Integration <a href="#e2ab" id="e2ab"></a>

Hint: the strength of your heroes determines how much $MAGIC you will earn in Elleria in the future.

# $MEDALS

### Medals ERC20 Token

![](<../../.gitbook/assets/Icon\_Medal-01 (2).png>)

**$MEDALS Address:** 0x000\
**Treasury (bridge) Address:** 0x000

MEDALS are a currency used for daily transactions within the City.

$MEDALS are needed for almost all in-game actions, such as strengthening heroes, repairing equipment, and preparing for quests.

You can obtain $MEDALS from in-game assignments.

### MEDALS is solely used within the game.

1 $MEDAL = 1 $MEDAL

### Total Supply: Uncapped

<table data-header-hidden><thead><tr><th></th><th width="150"></th><th width="150"></th><th></th></tr></thead><tbody><tr><td></td><td>MEDALS (%)</td><td>MEDALS ($)</td><td>Vesting</td></tr><tr><td>Ecosystem</td><td>-</td><td>∞</td><td>Unlocked</td></tr><tr><td>TreasureDAO (Seed)</td><td>-</td><td>-</td><td>-</td></tr><tr><td>Private sale</td><td>-</td><td>-</td><td>-</td></tr><tr><td>Team</td><td>-</td><td>-</td><td>-</td></tr><tr><td>Liquidity</td><td>-</td><td>10,000,000</td><td>Unlocked</td></tr><tr><td>Vault</td><td>-</td><td>100,000,000</td><td>Unlocked</td></tr></tbody></table>

###

### Medals Utility

Medals are considered game currency used to regulate the economy. $MEDALS are required to perform in-game actions to fully utilise your NFTs.

Medals in-game utility includes (but not limited to):

* Use $MEDALS to Upgrade Heroes.
* Use $MEDALS to Enter Quests
* Use $MEDALS for QOL/Cosmetic Upgrades
* Use $MEDALS to Upgrade Equipment
* Use $MEDALS to Keep houses/land operational
* Use $MEDALS to fortify and defend towns

Please note that the tokenomics are still a work in progress subject to changed before the release of $MEDALS.

####

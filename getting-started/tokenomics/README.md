# Tokenomics

## What are the tokens used in TELL?

Elleria has several tokens and NFTs that work together to create our Elleria's ecosystem.

* **Heroes** - NFT (base characters with progression system)
* **Equipment** - NFT (character progression)
* **Drops/Relics** - NFT (crafting/boosts/consumables/resources)
* **$ELLERIUM** (ELM) - Main Token
* **$MEDALS** (MEDALS) - Supplementary Token

### What is ELLERIUM ($ELM)

**ELLERIUM** is a rare natural resource found within the world of Elleria, said to be the crystallized blessing of the Goddess, and is being used as the main currency throughout the whole Metaverse.

$ELM can be obtained in-game by clearing quests and defeating bosses and is used to summon heroes, strengthen heroes, and create blessed equipment to allow for player progression.

{% content-ref url="usdellerium.md" %}
[usdellerium.md](usdellerium.md)
{% endcontent-ref %}

### What are MEDALS ($MEDALS)

**MEDALS** are a currency used for daily transactions within the City $MEDALS are not recognized across some parts of the Metaverse.

Almost all in-game actions, such as strengthening heroes, repairing equipment, and preparing for quests require $MEDALS.

{% content-ref url="usdmedals.md" %}
[usdmedals.md](usdmedals.md)
{% endcontent-ref %}

### What are Heroes (ERC721 NFTs)

**HEROES** are the souls of champions from other realms, summoned using Ellerium ($ELM). Heroes can exist as a soul (out of the game, without progression); or be given a physical body (sent into the game, with progression).

When given a physical body, Heroes can be sent on assignments for $MEDALS, which will be used for various actions, such as preparing for quests that can reward $ELM. The heroes' bodies can also be strengthened once they accumulate enough EXP.

{% content-ref url="../../gameplay-prologue/heroes/" %}
[heroes](../../gameplay-prologue/heroes/)
{% endcontent-ref %}

### What are Equipment (ERC721 NFTs)

**EQUIPMENT** are support items worn by heroes to improve their combat capabilities by increasing their attributes and combat stats. Different heroes will wear different kinds of equipment obtained from quests or the blacksmith.

With equipment, heroes can progress further and attempt more challenging quests, which reward better items, allowing for natural progression within the game.

{% content-ref url="../../gameplay-prologue/game-features/equipment.md" %}
[equipment.md](../../gameplay-prologue/game-features/equipment.md)
{% endcontent-ref %}

### What are Drops/Relics (ERC1155 NFTs)

**DROPS/RELICS** are precious artifacts found throughout Elleria. They have different uses ranging from passive effects, crafting ingredients, active consumables, and buffs.

These can range from simple materials dropped from basic monsters in quests to epic scrolls and legendary potions obtained from boss raids. They can also be obtained from divine gifts and crafting, which can permanently enhance your team's or an individual's capabilities when used appropriately.

### **Treasury & Safe Addresses**

All funds will be secured in a multi-signature wallet through Gnosis Safe. Three founding members will control these wallets, and any transaction will require 3 out of 4 (+1) signatures to execute (+1 key is locked in a safe for backup in case of ledger loss etc).

Doing so helps protect against hacks that target the treasuries, ensuring that there is no single point of failure. Additionally, it prevents any team member from having the power to do anything independently without the rest of the team being aware of and consenting to it. It also gives 100% transparency, as anyone can view all of the proposed transactions before they are executed and the history of every past transaction. You can see the addresses of these safes here:

**Safe Address:** 0x69832Af74774baE99D999e7F74FE3F7d5833bF84

Vesting & Lock addresses will be updated once available.

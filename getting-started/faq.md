---
description: >-
  If anything is left out, please reach out in the #ama channel in our Discord
  and we will add it in.
---

# FAQ

## General Questions

### What is Tales of Elleria?

Tales of Elleria (TELL) is a GameFi project, a metaverse, a market of rare utility-driven NFTs, and a liquidity pool (LP) opportunity that amalgamates in a three-dimensional world known as Elleria.

TELL will be launching as a P2E GameFi project in March 2022.

### What Blockchain is Tales of Elleria on?

TELL resides within Arbitrum One, an ETH Layer 2 using ETH as its native token. Please refer to the 'Connecting to Arbitrum' page for more information.

{% content-ref url="connecting-to-arbitrum/" %}
[connecting-to-arbitrum](connecting-to-arbitrum/)
{% endcontent-ref %}

### Are we free to play? What do we need to get started?

Players can explore Tales of Elleria by minting a free Ethereal Hero in-game. If you enjoy the experience and would like to receive more rewards, players can mint heroes from the Summoning Altar in-game (if a summoning event is available) or purchase heroes from the [Trove Marketplace](https://trove.treasure.lol/collection/tales-of-elleria) using $MAGIC.

The entry cost is dependent on your risk appetite and belief in the project. The more heroes you have or the stronger heroes you have, the more rewards you can accrue. There is an entire ecosystem built around heroes, and you can read the following pages to find out more about different play options and different features.

We recommend reading through the whitepaper to understand fully what you are getting into and how to reap the most rewards.

{% content-ref url="../gameplay-prologue/core-gameplay-loop/player-types.md" %}
[player-types.md](../gameplay-prologue/core-gameplay-loop/player-types.md)
{% endcontent-ref %}

{% content-ref url="../gameplay-prologue/heroes/" %}
[heroes](../gameplay-prologue/heroes/)
{% endcontent-ref %}

### Is the team doxxed?

The team is not officially doxxed, but we have made our appearance in several public events ;)

We have been building Tales of Elleria since December 2021 (launched April 2022), and have been participating actively in the community via text, discussions, AMAs, and events. If you don't think we're an honest and trustworthy team, pop into Discord and ask our community!

### How long has the team been in the space?

We've been involved in blockchain for a few years as consumers, and are making the transition to contributors now after grasping the landscape and finding that we can do way better than what's out there.

Our members have worked individually on other projects before, and some of our members have collaborated for a few years off-chain on several projects. We have a team consisting of experienced individuals coming from different fields such as game development, design, AR/VR, full-stack web development, marketing, business, and solidity development.

### How do I know this is a legitimate project?

Our NFT and game contracts are verified and open-source (Audited by 0xMacro). You can view our contract addresses from the Tokenomics sub-pages or our GitHub repository in [official-links.md](../references/official-links.md "mention") .

Our APIs are also public and accessible via GQL. Check this section for more information: [graphql-playground](../references/graphql-playground/ "mention")

### **Where is the open world?!**

Please refer to the Roadmap for our development plans! We are launching TELL in two components concurrently that will eventually be integrated; A web-based app (Phase Alpha), and an open-world game (Phase Beta).

{% content-ref url="../welcome-to-elleria/roadmap.md" %}
[roadmap.md](../welcome-to-elleria/roadmap.md)
{% endcontent-ref %}

## Regarding NFTs

### What are the Tokenomics used in TELL? How does it work?

Please refer to the Tokenomics section and its sub-pages!

{% content-ref url="tokenomics/" %}
[tokenomics](tokenomics/)
{% endcontent-ref %}

## Regarding Gameplay Features

See [guides](../welcome-to-elleria/guides/ "mention") for a quick start! \
Don't worry, you'll grasp the routine quickly

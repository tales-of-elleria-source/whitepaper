# Minting Heroes

To get started in Elleria, you will need a hero to accompany you!

Heroes can be purchased from Trove, or minted through our game.

### **Purchasing Heroes**

{% embed url="https://trove.treasure.lol/collection/tales-of-elleria" %}
Purchase heroes from Trove- Know what you're getting.
{% endembed %}

### **Minting Heroes**

#### Ethereal Heroes

New players will receive a prompt to visit the Summoning Altar and mint a FREE Ethereal Hero to kickstart their journey.&#x20;

{% content-ref url="../../gameplay-prologue/heroes/ethereal-heroes.md" %}
[ethereal-heroes.md](../../gameplay-prologue/heroes/ethereal-heroes.md)
{% endcontent-ref %}

#### Dawn Heroes

&#x20;Secondary Minting is midway but postponed for Ninja, check these articles for more information:

{% embed url="https://medium.com/@talesofelleria/joining-tales-of-elleria-2023-60d73371e841" %}
Summary on ToE, Visual Guide on Minting Process, and Sneak Peeks of Upcoming Features
{% endembed %}

{% content-ref url="dawn-heroes-minting-1.md" %}
[dawn-heroes-minting-1.md](dawn-heroes-minting-1.md)
{% endcontent-ref %}

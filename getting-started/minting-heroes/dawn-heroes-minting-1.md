# \[Archived] Dawn Heroes Auction

Dawn Heroes mint is ongoing via a weekly auction! 4 exclusive classes will be minted throughout 4 months; **250 heroes will be minted per week.**

**Machinists:** 12th December 2022 — 8th January 2023\
**Berserker:** 9th January 2023 — 5th February 2023\
**Spiritualist:** 6th February 2023 — 5th March 2023\
**Ninja:** 18th December 2023 until sold out

### **INCENTIVES FOR MINTING 💰** <a href="#cea1" id="cea1"></a>

1. Minters who bid 100 $ELM will **receive their heroes immediately**, FCFS, and **receive 20,000 $MEDALS** to help with hero upgrades. (upcoming implementation; will payback to previous eligible mints)
2. Minters with successful bids will receive a **1:1 ELM Shard ‘cashback’** that can be used in our crafting system. (upcoming implementation; will payback to previous eligible mints)
3. **If your winning bid was pledged with ≥ 50 $ELM, 1x Skillbook will be airdropped per hero** upon launch of the special attack system.
4. Chance to mint CS heroes/strong Questoors/Legendary Heroes at low prices. These heroes go for a premium on the Marketplace, and **could** be more cost-efficient to mint! RNGeesus 🍀

{% hint style="info" %}
All dawn heroes have a minimum stat of 10 points. Stat points are randomly generated on-chain! Dawn Heroes have the same functionality as Genesis Heroes- The difference lies in their maximum potential and stat distribution, making dawn heroes marginally weaker than Genesis Heroes, but with equal potential.
{% endhint %}

**Machinists Information**\
Machinists are **agile hard-hitting sharpshooters** that are slightly fragile due to their low vitality and endurance. **They excel against mobs of magical enemies with low resistance** due to their high max Intelligence and Will.

100 Stat: Intelligence (Main Stat)\
90 Stat: Agility (Sub Stat)\
75 Stat: Will\
60 Stat: Vitality/Endurance\
50 Stat: Strength

**Berserker Information**\
The Berserker is a ferocious and rugged brute who seeks the thrill of life through countless battles. He is a combat maniac, and can withstand any attacks his foes can throw at him. Berserker wields dual axes, ready to mow down enemies with **physical damage**.\
\
100 Stat: Vitality (Main Stat)\
90 Stat: Endurance (Sub Stat)\
75 Stat: Will\
60 Stat: Strength/Agility\
50 Stat: Intelligence

**Spiritualist Information**\
Mysterious, alluring and deadly. Holding a cursed voodoo doll, the Spiritualist conjures nether spirits to fight in her stead. The cursed damage inflicted by these spirits deals heavy **magical damage** and is said to defend well against physical attacks of the mortal realm.\
\
100 Stat: Intelligence (Main Stat)\
90 Stat: Endurance (Sub Stat)\
75 Stat: Vitality\
60 Stat: Will/Agility\
50 Stat: Strength

**Ninja Information**

Evildoers beware, the night is watching. The nimble Ninja blends into darkness and shadows to get where he needs to be, and his trusty katana slices all who dares to stand in his way. The ninja slices through enemies dealing **physical damage.**

100 Stat: Agility (Main Stat)\
90 Stat: Vitality (Sub Stat)\
75 Stat: Strength\
60 Stat: Will/Endurance\
50 Stat: Intelligence

<figure><img src="../../.gitbook/assets/image (5) (1) (1).png" alt=""><figcaption></figcaption></figure>

### Secondary Minting Mechanism

You will need these ERC20 tokens on Arbitrum One

**$USDC** (0xFF970A61A04b1cA14834A43f5dE4533eBDDB5CC8)\
**$ELLERIUM** (0xEEac5E75216571773C0064b3B591A86253791DB6)

**Minting costs 50 $USDC and a flexible amount of 0–100 $ELLERIUM.**\
Heroes will be distributed weekly on Sundays to the top 250 bids, prioritizing $ELM bid price.

All unsuccessful bids can be refunded manually from our website after each week’s auction ends until Tuesday.\\

### How to place bids and participate in minting <a href="#cf54" id="cf54"></a>

1. Head to our game site: [https://app.talesofelleria.com/](https://app.talesofelleria.com/).
2. Connect your wallet to our site.
3. Head to the **Summoning Altar.**

<figure><img src="https://miro.medium.com/max/700/1*MUmCDFtQAOvVhNBa7qdGGQ.png" alt=""><figcaption></figcaption></figure>

4\. Click on **Summon Heroes.** In the UI that appears, you can choose the number of heroes to bid for, and the amount of $ELM you are willing to pay per hero.

<figure><img src="https://miro.medium.com/max/700/1*8ATbKty3mjSfMqAOvm0yrA.png" alt=""><figcaption></figcaption></figure>

```
The auction is blind and data not revealed in our website. We recommend
choosing an $ELM amount you are comfortable with. To get a gauge on how
much $ELM you'd need to successfully mint, you can snoop around in the
Bid contract (0x7e2Ca894B8DdFd87758d684C7d20B729635994ce), ask around in
our Discord or check the Auction History for the previous floor price.
```

5\. After creating a bid, wait for the allocation that happens on Sunday at around 4 AM UTC to receive your heroes if successful! (Or if you max bid, you will receive the hero instantly)

<figure><img src="https://miro.medium.com/max/700/1*ZAMAMFmyChwPtOvO6PWgcw.png" alt=""><figcaption></figcaption></figure>

6\. (optional) To manage your bids, head to the **My Active Bids** tab (click on the Refresh button if your bids don’t show). You can view your bids here, and also increase bids/refund bids when eligible.

<figure><img src="https://miro.medium.com/max/700/1*mO6j9Mf72AyOx2A-LmBw6A.png" alt=""><figcaption></figcaption></figure>

7\. (optional) From **My Bid History**, you will be able to view past refunds and successful allocations. Click on a listing to view successfully allocated heroes for each bid!

<figure><img src="https://miro.medium.com/max/700/1*ANdP5r4eEA0_Nd1f5wLhyg.png" alt=""><figcaption></figcaption></figure>

```
We are still focusing on game development and the different milestones in
our roadmap. Secondary Minting was opened to allow new players to enter
and have a chance of obtaining CS/Strong Questoors at an entry-level cost.

In addition, there will be different dynamics involving different
classes in our combat system as it develops especially once the special 
attack system is out.

The different stats distribution offered in each class has its own pros/cons.
Differences will become more significant as the battle system is fledged out.
```

Good luck with your mints everyone! 🍀

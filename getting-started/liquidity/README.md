# Liquidity

Elleria offers different Staking options for passive rewards without requiring the users to own NFTs. Some staking pools are integrated into the world and offer the player advantages. Do refer to [Sustainable Liquidity](../tokenomics/sustainable-liquidity.md) for the macro idea of how staking will affect the Metaverse.

### $ELLERIUM-MAGIC LP Tokens Staking

{% content-ref url="usdelm-magic-lp.md" %}
[usdelm-magic-lp.md](usdelm-magic-lp.md)
{% endcontent-ref %}

### $ELLERIUM Single-Sided Staking

{% content-ref url="usdelm-staking.md" %}
[usdelm-staking.md](usdelm-staking.md)
{% endcontent-ref %}

# Runestones

Introducing Runestones, a feature in Tales of Elleria that revolutionizes gameplay by integrating liquidity tokens with a wealth of in-game benefits. Available through the Ellerian Bank, these tokens empower players to dramatically enhance their in-game experience.&#x20;

Runestones allow for the unlocking of exclusive abilities, advanced character enhancements, and other unique perks, giving players a substantial edge in their adventures, enriching player strategy and progression in unprecedented ways.

Runestones can be levelled by powering them with Liquidity Pool (LP) tokens. \
You can acquire LP tokens by providing liquidity on Magicswap [here](https://magicswap.lol/pools/0x3e8fb78ec6fb60575967bb07ac64e5fa9f498a4a/manage).

LP tokens used to power up Runestones will be held by the protocol and will not be sold. The goal of this feature is to build substantial Protocol Owned Liquidity, thereby maintaining the health of the token.&#x20;

<figure><img src="../../.gitbook/assets/image_2024-06-01_10-29-43.png" alt=""><figcaption></figcaption></figure>

{% hint style="warning" %}
**Runestone effects lasts for only one season of PVP**, each spanning one month.

Runestone levels achieved prior to the commencement of the first PvP season will be reset at the start of that season, which is more than one month out.&#x20;
{% endhint %}

<table><thead><tr><th width="91">Level</th><th width="157">Cost</th><th>Effect</th></tr></thead><tbody><tr><td>1</td><td>5 LP Tokens</td><td>Assignment Medals + 5%</td></tr><tr><td>2</td><td>10 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 10%</td></tr><tr><td>3</td><td>20 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 20%<br>Combat Stats + 1%</td></tr><tr><td>4</td><td>40 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 25%<br>Exploration Emissions + 10%<br>Combat Stats + 2%</td></tr><tr><td>5</td><td>80 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 30%<br>Exploration Emissions + 15%<br>Combat Stats + 4%</td></tr><tr><td>6</td><td>160 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 35%<br>Exploration Emissions + 20%<br>Combat Stats + 6%</td></tr><tr><td>7</td><td>320 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 40%<br>Exploration Emissions + 25%<br>Combat Stats + 8%<br>PVP rewards + 10%</td></tr><tr><td>8</td><td>640 LP Tokens</td><td>Assignment Medals + 10%<br>Quest Relics Drop Rate + 50%<br>Exploration Emissions + 25%<br>Combat Stats + 10%<br>PVP rewards + 20%</td></tr></tbody></table>

The increase in Exploration Emissions is a multiplier of your stake earned, and will not affect the earnings of others. \
\
Players, be sure to carefully consider the cost of levelling your Runestones against the benefits they provide. Strategic choices will enhance your gameplay experience in Tales of Elleria!

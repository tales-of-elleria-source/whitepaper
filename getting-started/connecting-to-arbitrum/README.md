# Connecting to Arbitrum

## Elleria x Arbitrum

![](<../../.gitbook/assets/Blog Arbitrum Logo.png>)

### What is Arbitrum?

[Arbitrum](https://arbitrum.io/) is an Optimistic Rollup protocol designed for the Ethereum blockchain. The Arbitrum Rollup chain is a super scaled Layer 2 (L2) chain, and interacting with Arbitrum feels exactly like interacting with Ethereum without compromising on security or decentralization, except:

* **Transactions are significantly faster.**
* **Gas fees are significantly lower.**

### How do I get started on Arbitrum?

{% hint style="info" %}
**Note:** Arbitrum's native token is also $ETH. To use your ETH on Arbitrum mainnet, you will need to bridge ETH from Ethereum Mainnet to Arbitrum One Mainnet.
{% endhint %}

Arbitrum has an official tutorial on bridging ETH from Ethereum Mainnet into Arbitrum.

{% embed url="https://arbitrum.io/bridge-tutorial" %}
Official Arbitrum Bridge Tutorial
{% endembed %}

#### Installing Metamask

To get started, you first need a Web3 wallet, such as [Metamask](https://metamask.io/).

{% embed url="https://metamask.io" %}
Metamask supports Chrome/Firefox/Brave/Edge; iOS; Android
{% endembed %}

#### Adding Arbitrum One to your Metamask

Once Metamask has been Installed, you will need to add the Arbitrum One network.

{% tabs %}
{% tab title="Arbitrum One (Mainnet)" %}
1. From Metamask, click on your profile picture, and click on "Settings".
2. Click on "Networks - Add and edit custom RPC networks".
3. Click on "Add Network".
4. Use the following details to add Arbitrum One Mainnet to your Metamask.
5. Network Name: Arbitrum One Mainnet
6. RPC: [https://arb1.arbitrum.io/rpc](https://arb1.arbitrum.io/rpc)
7. Chain ID: 42161
8. Currency Symbol: ETH
9. Block Explorer URL: [https://arbiscan.io](https://arbiscan.io/)
{% endtab %}

{% tab title="Arbitrum Testnet" %}
{% hint style="warning" %}
All funds on testnet are for testing and do not have any monetary value. You can get free ETH tokens from the following faucets.\
\
1\. Chainlink faucet: [https://faucets.chain.link/rinkeby](https://faucets.chain.link/rinkeby)

2\. Rinkeby faucet: [https://faucet.rinkeby.io/](https://faucet.rinkeby.io/)
{% endhint %}

1. From Metamask, click on your profile picture, and click on "Settings".
2. Click on "Networks - Add and edit custom RPC networks".
3. Click on "Add Network".
4. Use the following details to add Arbitrum One Mainnet to your Metamask.
5. Network Name: Arbitrum Testnet
6. RPC URL: [https://rinkeby.arbitrum.io/rpc](https://rinkeby.arbitrum.io/rpc)
7. ChainID: 421611
8. Symbol: ETH
9. Block Explorer URL: [https://testnet.arbiscan.io](https://rinkeby-explorer.arbitrum.io/#/)
{% endtab %}
{% endtabs %}

#### Bridging ETH into Arbitrum One

Head to the Arbitrum One Bridge:

{% embed url="https://bridge.arbitrum.io" %}

1. Login to your Metamask account, and switch to your Arbitrum network.
2. Enter the amount of ETH you want to bridge over, and click on "Deposit".
3. Read the pop up alert, and click "Deposit" again.
4. To withdraw, please refer to "[Bridging back to Mainnet](bridging-back-to-mainnet.md)".

{% hint style="warning" %}
**Warning:** Make sure to bridge additional ETH to pay for transactions on Arbitrum.
{% endhint %}

### How to trade Tales of Elleria Assets

Elleria is partnered with TreasureDAO, and uses their Marketplace and DEX. You can find the links to trade our assets here:

{% embed url="https://magicswap.lol/?input=MAGIC&output=ELM" %}
Swap for $ELM here, the token that powers our ecosystem
{% endembed %}

{% embed url="https://app.treasure.lol/collection/tales-of-elleria" %}
Obtain Heroes and start playing!
{% endembed %}


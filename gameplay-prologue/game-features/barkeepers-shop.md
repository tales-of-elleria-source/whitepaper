# Barkeeper's Shop

Welcome! The Barkeeper has some special items on sale just for you. Some of these items aren't readily available through questing or from any other sources.

## Shop Tabs

There are currently 2 main tabs within the shop:

**Elleria's Bounty** - Items purchased with in-game $ELM.

**Dimensional Haul -** Items purchased with currency from across the blockchain, requiring different on-chain tokens/resources.

### Elleria's Bounty

Items under Elleria's Bounty are off-chain transactions, and take place after a message is signed. The items purchasable are only purchasable using in-game currencies.

| Item List                      | Cost    |
| ------------------------------ | ------- |
| Ellerium Shard (untradeable)   | 10 $ELM |
| Diluted Recovery Potion        | 20 $ELM |
| Recovery Potion                | 50 $ELM |
| Medals Exchange Ticket 1 (1k)  | 5 $ELM  |
| Medals Exchange Ticket 2 (5k)  | 25 $ELM |
| Medals Exchange Ticket 3 (20k) | 95 $ELM |
| Medals Exchange Ticket 4 (???) | 25 $ELM |

### Dimensional Haul

Items under Dimensional Haul are **on-chain transactions**, where the shopkeeper sells items purchasable only using $MAGIC and other related on-chain tokens.

You are required to approve token spending the first time you purchase an item.

This feature allows for cross-metaverse collaboration options where partner projects can use their NFTs/Tokens/Resources from across the blockchain to purchase relics and other items used within Elleria.

| Item List                                                                                                       | Cost       |
| --------------------------------------------------------------------------------------------------------------- | ---------- |
| 1x Magic Shard (untradeable)                                                                                    | 1 $MAGIC   |
| 1x Boss Entry Ticket                                                                                            | 5 $MAGIC   |
| <p><strong>Beginner Pack</strong><br>20,000 $MEDALS<br>5x Faded Memories<br>5x Diluted Recovery Potions</p>     | 20 $MAGIC  |
| <p><strong>Novice Pack</strong><br>50,000 $MEDALS<br>2x Vivid Memories<br>5x Recovery Potions</p>               | 50 $MAGIC  |
| <p><strong>Master Pack</strong><br>125,000 $MEDALS<br>6x Vivid Memories<br>5x Potent Recovery Potions</p>       | 100 $MAGIC |
| <p><strong>Supreme Pack</strong><br>250,000 $MEDALS<br>5x Timeless Memories<br>5x Superior Recovery Potions</p> | 200 $MAGIC |

## Limited Items

Some items are time-limited and refreshed daily! You can see the remaining quantity under each item.

Some items might be limited edition and will never return once expired too. Keep a lookout for our Discord announcement when these items appear.

{% hint style="info" %}
The shop refreshes at 0400 UTC, together with quest resets. You can see the time until refresh on the bottom left.
{% endhint %}

<figure><img src="../../.gitbook/assets/image (36).png" alt=""><figcaption></figcaption></figure>

# Wishing Well

The wishing well has been live since `1st January 2023 4am UTC` and is accessible from the World Map via the Town Square!

<figure><img src="../../.gitbook/assets/image (30) (1) (1).png" alt=""><figcaption></figcaption></figure>

### Relics Recycling & Conversion

If you have too many relics that are unused and you do not want to list them on the Marketplace, the Wishing Well is an option that allows you to obtain 1 new relic by burning 3 relics.

**Only tradeable relics Tier 4 and under can be burnt. You will receive loot based on the lowest tier relic burnt.**

{% hint style="warning" %}
Burning 3 T2 relics will use the Tier 2 and 3 table.

Burning 1 T1 and 2 T2 relics will use the Tier 1 table instead.
{% endhint %}

There is a different loot table for different tiers of relics burnt, as follows:

{% tabs %}
{% tab title="Tier 1" %}
{% hint style="info" %}
This lists the possible rewards from burning T1 relics.\
(Rewards can have mixed tiers!)
{% endhint %}

<table data-column-title-hidden data-view="cards"><thead><tr><th>Tier 1 Obtainable Loot</th></tr></thead><tbody><tr><td>Cursed Scroll</td></tr><tr><td>Risky Scroll</td></tr><tr><td>Ruby Chip</td></tr><tr><td>Sapphire Chip</td></tr><tr><td>Emerald Chip</td></tr><tr><td>Topaz Chip</td></tr><tr><td>Diamond Chip</td></tr><tr><td>Amethyst Chip</td></tr><tr><td>Copper Powder</td></tr><tr><td>Zinc Powder</td></tr><tr><td>Iron Powder</td></tr><tr><td>Tin Powder</td></tr><tr><td>Diluted Recovery Potion</td></tr><tr><td>Goblin Leader's Belt</td></tr><tr><td>Vial of Mysterious Blood</td></tr><tr><td>Weird Parchment</td></tr><tr><td>Hopgoblin's Helmet</td></tr><tr><td>Cloth Strips</td></tr><tr><td>Wood Chips</td></tr><tr><td>Goblin's Foot</td></tr><tr><td>Goblin's Hand</td></tr><tr><td>Goblin's Ear</td></tr><tr><td>Goblin's Magic Necklace</td></tr><tr><td>Leather</td></tr><tr><td>Goblin's Bone Necklace</td></tr><tr><td>Goblin's Leather Bundle</td></tr><tr><td>Goblin's Bones</td></tr><tr><td>Bone Powder</td></tr><tr><td>Small Goblin Insignia</td></tr></tbody></table>
{% endtab %}

{% tab title="Tier 2 and 3" %}
{% hint style="info" %}
This lists the possible rewards from burning T2/T3 relics.\
(Rewards can have mixed tiers!)
{% endhint %}

<table data-column-title-hidden data-view="cards"><thead><tr><th>Lesser Chaos Scroll</th></tr></thead><tbody><tr><td>Lesser Chaos Scroll</td></tr><tr><td>Slime Drop</td></tr><tr><td>Ruby Shard</td></tr><tr><td>Sapphire Shard</td></tr><tr><td>Emerald Shard</td></tr><tr><td>Topaz Shard</td></tr><tr><td>Diamond Shard</td></tr><tr><td>Amethyst Shard</td></tr><tr><td>Copper Ore</td></tr><tr><td>Zinc Ore</td></tr><tr><td>Iron Ore</td></tr><tr><td>Tin Ore</td></tr><tr><td>Goblin King's Axe</td></tr><tr><td>Goblin Mage's Staff</td></tr><tr><td>Recovery Potion</td></tr><tr><td>Big Goblin Insignia</td></tr><tr><td>Three-leaf Clover</td></tr><tr><td>Hopgoblin's Shield</td></tr><tr><td>Hopgoblin's Axe</td></tr><tr><td>Goblin's Club</td></tr></tbody></table>
{% endtab %}

{% tab title="Tier 4" %}
{% hint style="info" %}
This lists the possible rewards from burning T4 relics.\
(Rewards can have mixed tiers!)
{% endhint %}

<table data-column-title-hidden data-view="cards"><thead><tr><th>Chaos Scroll</th></tr></thead><tbody><tr><td>Chaos Scroll</td></tr><tr><td>Potent Recovery Potion</td></tr><tr><td>Vial of Holy Water</td></tr><tr><td>Washed Parchment</td></tr><tr><td>Goblin Leader's Crown</td></tr><tr><td>Ruby Shard</td></tr><tr><td>Sapphire Shard</td></tr><tr><td>Emerald Shard</td></tr><tr><td>Topaz Shard</td></tr><tr><td>Diamond Shard</td></tr><tr><td>Amethyst Shard</td></tr><tr><td>Copper Ore</td></tr><tr><td>Zinc Ore</td></tr><tr><td>Iron Ore</td></tr><tr><td>Tin Ore</td></tr><tr><td>Four-leafed Clover</td></tr><tr><td>Lesser Chaos Scroll</td></tr><tr><td>Recovery Potion</td></tr><tr><td>Big Goblin Insignia</td></tr><tr><td>Three-leaf Clover</td></tr><tr><td>Slime Drop</td></tr><tr><td>Hopgoblin's Shield</td></tr><tr><td>Hopgoblin's Axe</td></tr></tbody></table>
{% endtab %}
{% endtabs %}

### How to use Wishing Well

1. Head to the Town Square.

<figure><img src="../../.gitbook/assets/image (6) (2).png" alt=""><figcaption></figcaption></figure>

2\. Click on 'Wishing Well'

<figure><img src="../../.gitbook/assets/image (29) (1).png" alt=""><figcaption></figcaption></figure>

3\. Choose 3 spare relics to burn. These relics can be repeated (2/3 of the same item). Choose how many sets of the relics you want to burnt. Clicking 'max' will auto calculate the maximum sets of relics you have.\
\
Click on 'Make Wish' once confirmed, good luck! :four\_leaf\_clover:

<figure><img src="../../.gitbook/assets/image (28) (1).png" alt=""><figcaption></figcaption></figure>

4\. You will have to click on 'Throw', and sign a message to authenticate and confirm burning of your relics. We don't want you accidentally burning important items!

<figure><img src="../../.gitbook/assets/image (1) (1) (1) (2).png" alt=""><figcaption></figcaption></figure>

5\. Check your loot!

<figure><img src="../../.gitbook/assets/image (31).png" alt=""><figcaption></figcaption></figure>

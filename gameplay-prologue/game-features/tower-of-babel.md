# Tower of Babel

Located in the Colosseum on the left of the Main Menu, the Tower of Babel is filled with challenging monsters that will test your skills and strategy like never before. Enticing prizes await you on each cleared floor, and a bounty of $ELM awaits you for placing high in the Leaderboards!

**The Tower's gates opened on 19th June 2023!**

<figure><img src="../../.gitbook/assets/photo1679468844 (1).jpeg" alt=""><figcaption><p>Tower of Babel</p></figcaption></figure>

## **Tower of Babel Mechanics** <a href="#id-8393" id="id-8393"></a>

* The tower has **46 Floors.**
* Each cycle lasts for 2 weeks (resets bi-weekly).
* Rewards, difficulty, and score obtained **increases** the higher the floor cleared.
* Every floor's first clear reward **resets every cycle**.
* Every hero has **TWO FREE attempts** to enter the tower every cycle.
* Players can spend 2\*n $ELM for more attempts. (n = num. of exceeded attempts)

{% hint style="info" %}
In addition to these rewards, accumulating score places you in the Babel leaderboards that award $ELM! More information in the section below. Keep reading :point\_down:
{% endhint %}

<figure><img src="../../.gitbook/assets/photo1679657215 (4).jpeg" alt=""><figcaption><p>1st Floor</p></figcaption></figure>

### Biweekly First Clear Reward List

Below is the full reward list attainable upon clearing each floor:

<table><thead><tr><th width="90.33333333333331">Floor</th><th width="109">Rec. Level</th><th width="308">Monsters</th><th>Clear Rewards</th></tr></thead><tbody><tr><td>1</td><td>Level 5</td><td>Slime Friend</td><td>200 $MEDALS</td></tr><tr><td>2</td><td>Level 5</td><td>Trainee Goblin</td><td>1x Weird Parchment</td></tr><tr><td>3</td><td>Level 5</td><td>Gob Warrior</td><td>1x Vial of Mysterious Blood</td></tr><tr><td>4</td><td>Level 5</td><td>Slime Foe</td><td>300 $MEDALS</td></tr><tr><td>5</td><td>Level 10</td><td>Gob Mage</td><td>5x ELM Shards</td></tr><tr><td>6</td><td>Level 10</td><td>Plant Slime</td><td>1x Diluted Recovery Potion (SB)</td></tr><tr><td>7</td><td>Level 10</td><td>Hopgoblin</td><td>1x Diluted Recovery Vial (SB)</td></tr><tr><td>8</td><td>Level 15</td><td>Lava Slime</td><td>1000 $MEDALS</td></tr><tr><td>9</td><td>Level 15</td><td>Gob Mage + Hopgoblin</td><td>1x Recovery Potion (SB)</td></tr><tr><td>10</td><td>Level 15</td><td>Plant Slime + Lava Slime</td><td>1x Lesser Chaos Scroll (SB)</td></tr><tr><td>11</td><td>Level 20</td><td>Hopgoblin + Gob Leader</td><td>1x Vial of Holy Water</td></tr><tr><td>12</td><td>Level 20</td><td>Lava Slime + Slime King</td><td>1x Washed Parchment</td></tr><tr><td>13</td><td>Level 25*</td><td>Gob Mage + Plant Slime + Gob Leader</td><td>3000 $MEDALS</td></tr><tr><td>14</td><td>Level 25*</td><td>Hopgoblin + Lava Slime + Slime King</td><td>20x ELM Shards</td></tr><tr><td>15</td><td>Level 30*</td><td>Goblin Ogre</td><td>1x Lesser Positive Scroll (SB)</td></tr><tr><td>16</td><td>Level 5</td><td>Slime Friend</td><td>1x Slime Drop</td></tr><tr><td>17</td><td>Level 10</td><td>Trainee Goblin</td><td>1x Topaz Chip</td></tr><tr><td>18</td><td>Level 15</td><td>Gob Warrior</td><td>1x Emerald Chip</td></tr><tr><td>19</td><td>Level 20</td><td>Slime Foe</td><td>2000 $MEDALS</td></tr><tr><td>20</td><td>Level 25</td><td>Gob Mage</td><td>10x ELM Shards</td></tr><tr><td>21</td><td>Level 25</td><td>Plant Slime</td><td>1x Slime Goo</td></tr><tr><td>22</td><td>Level 25</td><td>Hopgoblin</td><td>1 x Chaos Scroll (SB)</td></tr><tr><td>23</td><td>Level 25</td><td>Lava Slime</td><td>1x Three-leaf Clover</td></tr><tr><td>24</td><td>Level 30</td><td>Gob Mage + Hopgoblin</td><td>3000 $MEDALS</td></tr><tr><td>25</td><td>Level 30</td><td>Plant Slime + Lava Slime</td><td>2x Random Elixir (SB)</td></tr><tr><td>26</td><td>Level 35</td><td>Hopgoblin + Gob Leader</td><td>2x Recovery Vial (SB)</td></tr><tr><td>27</td><td>Level 35</td><td>Lava Slime + Slime King</td><td>3x Potent Recovery Vial (SB)</td></tr><tr><td>28</td><td>Level 40*</td><td>Gob Mage + Plant Slime + Gob Leader</td><td>6000 $MEDALS</td></tr><tr><td>29</td><td>Level 40*</td><td>Hopgoblin + Lava slime + Slime King</td><td>40x ELM Shards</td></tr><tr><td>30</td><td>Level 50*</td><td>Goblin Ogre</td><td>1x Blessing Scroll (SB)</td></tr><tr><td>31</td><td>Level 5</td><td>Slime Friend</td><td>2000 $MEDALS</td></tr><tr><td>32</td><td>Level 15</td><td>Trainee Goblin</td><td>3x Three-leaf Clover</td></tr><tr><td>33</td><td>Level 20</td><td>Gob Warrior</td><td>1x Potent Recovery Potion (SB)</td></tr><tr><td>34</td><td>Level 25</td><td>Slime Foe</td><td>3000 $MEDALS</td></tr><tr><td>35</td><td>Level 30</td><td>Gob Mage</td><td>15x ELM Shards</td></tr><tr><td>36</td><td>Level 35</td><td>Plant Slime</td><td>1x Slime Essence</td></tr><tr><td>37</td><td>Level 35</td><td>Hopgoblin</td><td>1x Cursed Scroll (SB)</td></tr><tr><td>38</td><td>Level 35</td><td>Lava Slime</td><td>1x Four-leafed Clover</td></tr><tr><td>39</td><td>Level 40</td><td>Gob Mage + Hopgoblin</td><td>5000 $MEDALS</td></tr><tr><td>40</td><td>Level 40</td><td>Plant Slime + Lava Slime</td><td>3x Random Elixir (SB)</td></tr><tr><td>41</td><td>Level 45</td><td>Hopgoblin + Gob Leader</td><td>2x Potent Recovery Potion (SB)</td></tr><tr><td>42</td><td>Level 45*</td><td>Lava Slime + Slime King</td><td>60x ELM Shards</td></tr><tr><td>43</td><td>Level 50*</td><td>Gob Mage + Plant Slime + Gob Leader</td><td>1x Jester’s Tricks</td></tr><tr><td>44</td><td>Level 50*</td><td>Hopgoblin + Lava Slime + Slime King</td><td>1x Random Skin Box</td></tr><tr><td>45</td><td>Level 60*</td><td>Gob leader + King Slime + Goblin Ogre</td><td>1x Miracle Scroll (SB)</td></tr><tr><td>46</td><td>Level 80*</td><td>Zorag the Goblin King</td><td>1x Miracle Scroll</td></tr></tbody></table>

{% hint style="info" %}
\*Some stages are harder than others, be prepared with elixirs and potions!
{% endhint %}

### Leaderboard Rewards <a href="#id-22af" id="id-22af"></a>

In addition to earning rewards for successfully clearing each floor in the Tower of Babel, Heroes will be **evaluated based on their performance** and ranked on a leaderboard, displaying their position among the top players. This leaderboard will also serve as a platform for distributing **$ELM** and other prizes.

<figure><img src="../../.gitbook/assets/image (14) (1).png" alt=""><figcaption><p>Tower of Babel Leaderboard</p></figcaption></figure>

There will be a total of **8** leaderboards, one per class.

Each class's leaderboard will remain **completely separate** from the others.

A prize pool of **3500 $ELM** will be **equally divided** among the eight classes every cycle.

Players will be ranked according to the total score accumulated per class.

Each Hero will be given a score based on the following scoring system:

<table><thead><tr><th width="234">Criteria</th><th>Score</th></tr></thead><tbody><tr><td>Stage Clear</td><td>Floor * 1000 (eg. Floor 10 gives 10,000 score)</td></tr><tr><td>Turns Bonus</td><td>1000 - 100 per turn (min 0)</td></tr><tr><td>HP Bonus</td><td>% Remaining HP * 10</td></tr><tr><td>Overkill Bonus</td><td>Excess Damage (all monsters) * 10</td></tr><tr><td>Repeat Bonus</td><td>1% bonus per past clear, max 20%</td></tr></tbody></table>

Below is the breakdown of the Leaderboard rewards that will be distributed.

| Placing | % Prize Pool |
| ------- | ------------ |
| 1st     | 2.50%        |
| 2nd     | 2.00%        |
| 3rd     | 1.50%        |
| 4-10    | 1.25%        |
| 11-25   | 1.00%        |
| 26-50   | 0.75%        |
| 50-100  | 0.50%        |
| 100-200 | 0.25%        |
| Special | 1.5%         |
| Total   | 100%         |

If you’re up for the challenge, be the player who ranks the highest on the Leaderboard to qualify for these rewards!

{% hint style="info" %}
To prevent exploits, heroes can only enter the Tower of Babel for **one account per cycle**. If your hero enters the ToB at least once, and you sell/send the hero to another wallet, the new owner will not be able to see his hero in the ToB until the next cycle. A new trait ("Babel Entry") has been added to allow buyers to check whether Tower of Babel can be entered if a hero is bought.
{% endhint %}

## Feature Guide

### **Entering the Tower of Babel**

1. Click on "Tower of Babel" on the world map

<figure><img src="../../.gitbook/assets/image (23) (2).png" alt=""><figcaption><p>Tower of Babel World Map</p></figcaption></figure>

2. Click "Enter Tower".

<figure><img src="../../.gitbook/assets/image (11) (3).png" alt=""><figcaption><p>Enter Tower of Babel</p></figcaption></figure>

3. Select the floor that you want to enter. You can unlock the next floor by clearing the previous.

<figure><img src="../../.gitbook/assets/image (4) (2).png" alt=""><figcaption><p>Select floor to enter</p></figcaption></figure>

4. Select the heroes you want to send to complete the floor. Remember that higher floors generally award more score!

<figure><img src="../../.gitbook/assets/image (30) (1).png" alt=""><figcaption></figcaption></figure>

5. Click on "Embark on Quest " and authorize the transactions. Good luck!

<figure><img src="../../.gitbook/assets/image (13) (2).png" alt=""><figcaption></figcaption></figure>

6. You can view rewards and obtain a score after clearing each floor. The class leaderboard ranking is updated immediately after every clear.

<figure><img src="../../.gitbook/assets/image (22) (2).png" alt=""><figcaption></figcaption></figure>

7. Click on the (i) if you want to view the score breakdown to strategize.

<figure><img src="../../.gitbook/assets/image (5) (2).png" alt=""><figcaption></figcaption></figure>

### **Viewing the Tower of Babel Leaderboard**

1. Click on "Tower of Babel" on the world map.

<figure><img src="../../.gitbook/assets/image (9) (1).png" alt=""><figcaption><p>Tower of Babel World Map</p></figcaption></figure>

2. Click on "Tower Leaderboards".

<figure><img src="../../.gitbook/assets/image (12) (1).png" alt=""><figcaption><p>Tower Leaderboards</p></figcaption></figure>

3. Use the arrows to switch between different class leaderboards.

<figure><img src="../../.gitbook/assets/image (3) (2).png" alt=""><figcaption></figcaption></figure>

4. Click on the tabs: "Leaderboard", "$ELM Distribution" or "Hall Of Fame" to see the different hero rankings for the class. You can select any hero to view its stats.

<figure><img src="../../.gitbook/assets/image (60).png" alt=""><figcaption></figcaption></figure>

5. Select "$ELM Ladder" to view the score threshold for different rankings. This updates every 15 minutes!

<figure><img src="../../.gitbook/assets/image (8) (1).png" alt=""><figcaption></figcaption></figure>

Good luck fellow Ellerians. Prove that you are the mightiest of all by reaching the top of the Tower of Babel. May Elysis guide your way!

{% hint style="info" %}
All information in Babel is subject to change over different cycles to balance emissions and adjust difficulty as necessary!
{% endhint %}

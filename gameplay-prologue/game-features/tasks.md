# Tasks

## Daily/Weekly Tasks

Daily/weekly tasks are meant to be cleared with a **single hero** without any extra effort, allowing **all players** to clear them as long as they play the game daily!

Daily/weekly tasks will automatically be completed as you perform actions in-game. However, make sure to claim before the week resets on Monday at 0400 UTC, or your unclaimed rewards will be lost!

You can view tasks and claim rewards from the 'Tasks' button on the World Map:

<figure><img src="../../.gitbook/assets/image (5).png" alt=""><figcaption></figcaption></figure>

### **Reward for Daily Tasks:**

| Day       | Reward                        |
| --------- | ----------------------------- |
| Monday    | 1x Diluted Potion (Soulbound) |
| Tuesday   | 100 $MEDALS                   |
| Wednesday | 1x T1 Goblin Component        |
| Thursday  | 200 $MEDALS                   |
| Friday    | 1 Ellerium Shard              |
| Saturday  | 200 $MEDALS                   |
| Sunday    | 1x Random Elixir (Soulbound)  |

<figure><img src="../../.gitbook/assets/image (33).png" alt=""><figcaption></figcaption></figure>

### **Reward for Weekly Task:**

* 10x Ellerium Shards (Soulbound)
* 1x Lesser Positive Scroll (Soulbound) - Gives +1 to 1 random stat!

<figure><img src="../../.gitbook/assets/image (34).png" alt=""><figcaption></figcaption></figure>

### **FAQ:**

**1. When do weekly tasks reset?**

Weekly tasks reset on Monday's reset. (when gold rush disappears)

**2. I forgot to claim for a previous day!!**

You can leave all your tasks and claim them at the end of the week (Sunday) before the weekly reset, tasks should automatically be tracked daily even if you do not open the task page.

**3. What does soulbound mean?**

The relic will be untradeable (cannot unbind from account). For scrolls, the stats will stay with your hero only within the account.



## Adventurer's Guidebook

<figure><img src="../../.gitbook/assets/image (1) (1).png" alt=""><figcaption></figcaption></figure>

The Adventurer's Guidebook in Tales of Elleria serves as a comprehensive roadmap for new players, detailing a series of structured tasks designed to facilitate progression through the game. This guide not only introduces users to the essential mechanics and features of Elleria but also provides strategic advice on character development and account enhancement.&#x20;

By following the guidebook, players can efficiently navigate the complexities of the game, optimizing their journey and enhancing their overall gaming experience through rewards. This tool is crucial for ensuring that newcomers can quickly become competent and competitive within the vibrant world of Tales of Elleria.



<table><thead><tr><th width="118">Chapter</th><th>Task</th><th>Reward</th></tr></thead><tbody><tr><td>1</td><td>Summon 1 Ethereal Hero at the Altar</td><td>3x Fading Memories</td></tr><tr><td>1</td><td>Clear Floor 1 of Tower of Babel</td><td>10,000 $MEDALS</td></tr><tr><td>1</td><td>Upgrade a Hero from the Heroes' Quarters</td><td>5,000 $MEDALS</td></tr><tr><td>1</td><td>Earn 100 $MEDALS from Assignments</td><td>2x Diluted Recovery Vials</td></tr><tr><td>1</td><td>Complete 1 Daily Task</td><td>25x Crafting EXP</td></tr><tr><td>1</td><td>Open 3 Wooden Lootboxes</td><td>10x Soul Dust</td></tr><tr><td>2</td><td>Purchase 1 Item from the Shop</td><td>5x Ellerium Shards</td></tr><tr><td>2</td><td>Use the Relic Crafting System</td><td>10x Ellerium Shards</td></tr><tr><td>2</td><td>Use the Wishing Well</td><td>100x Crafting EXP</td></tr><tr><td>2</td><td>Use a Consumable in Battle</td><td>1 Luck Elixir</td></tr><tr><td>2</td><td>Complete 1 Weekly Task</td><td>3x Recovery Vials</td></tr><tr><td>2</td><td>Use a Scroll on a Hero</td><td>50x Ellerium Shards</td></tr><tr><td>2</td><td>Clear Floor 5 of Proving Grounds</td><td>Ethereal-CS Scroll <br>(turns an ethereal hero into 86/61)</td></tr><tr><td>3</td><td>Craft any Equipment</td><td>1x Random T3 Gem/Metal</td></tr><tr><td>3</td><td>Upgrade an Equipment to +8</td><td>1x Random T3 Gem/Metal</td></tr><tr><td>3</td><td>Use Equipment Reroll</td><td>20x Ellerium Shards</td></tr><tr><td>3</td><td>Infuse an Equipment</td><td>20x Ellerium Shards</td></tr><tr><td>3</td><td>Clear Floor 5 of Tower of Babel</td><td>3x Recovery Vials</td></tr><tr><td>3</td><td>Clear Floor 10 of Tower of Babel</td><td>3x Potent Recovery Vials</td></tr><tr><td>4</td><td>??</td><td>??</td></tr></tbody></table>

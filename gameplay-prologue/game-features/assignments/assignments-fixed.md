# Assignments (Fixed)

Assignments emit a fixed amount of MEDALS per day. \
Heroes sent to assignments will become busy and be unable to use other game features.\
\
Jobs have different requirements as listed below:

<table><thead><tr><th width="252">Job Title</th><th>Requirements</th><th>MEDALS per second</th></tr></thead><tbody><tr><td><strong>Misc. Tasks</strong></td><td>No Requirements</td><td>(0.1 / 60) $MEDALS</td></tr><tr><td>Guard Duty</td><td>Warriors Only;<br>Level Required: 10<br>Strength >= 86<br>Vitality >= 61</td><td>(0.2 / 60) + (Hero's Strength - 85) * (0.05 / 60) MEDALS</td></tr><tr><td>Enemy Recon</td><td>Assassins Only;<br>Level Required: 10<br>Agility >= 86<br>Strength >= 61</td><td>(0.2 / 60) + (Hero's Agility - 85) * (0.05 / 60) MEDALS</td></tr><tr><td>Magic Research</td><td>Mages Only;<br>Level Required: 10<br>Intelligence >= 86<br>Will >= 61</td><td>(0.2 / 60) + (Hero's Intelligence - 85) * (0.05 / 60) MEDALS</td></tr><tr><td>Hunting Game</td><td>Rangers Only;<br>Level Required: 10<br>Strength >= 86<br>Agility >= 61</td><td>(0.2 / 60) + (Hero's Strength - 85) * (0.05 / 60) MEDALS</td></tr><tr><td>Altar Maintenance</td><td>Spiritualists Only;<br>Level Required: 10<br>Intelligence >= 86<br>Endurance >= 61</td><td>(0.2 / 60) + (Hero's Intelligence - 85) * (0.05 / 60) MEDALS</td></tr><tr><td>City Patrol</td><td>Berserkers Only;<br>Level Required: 10<br>Vitality >= 86<br>Endurance >= 61</td><td>(0.2 / 60) + (Hero's Vitality - 85) * (0.05 / 60) MEDALS</td></tr><tr><td>Ruins Scouting</td><td>Machinists Only;<br>Level Required: 10<br>Intelligence >= 86<br>Agility >= 61</td><td>(0.2 / 60) + (Hero's Intelligence - 85) * (0.05 / 60) MEDALS</td></tr><tr><td></td><td></td><td></td></tr></tbody></table>

{% hint style="info" %}
The community has coined the term CS for class-specific jobs requiring 86 main stat and 61 sub stat.
{% endhint %}

For heroes that have good stats for questing yet are unable to qualify for a fixed emission job, we will introduce pool-based assignments that has more general requirements. Please refer to the next page for more information.

### Emissions Multiplier

Experienced heroes are more efficient and will have better rewards from assignments. Your heroes are entitled to an emissions multiplier depending on their levels:

| Level    | Emissions Multiplier |
| -------- | -------------------- |
| 1 to 19  | 1x                   |
| 20 to 29 | 2x                   |
| 30 to 39 | 3x                   |
| 40 to 49 | 6x                   |
| 50 to 59 | 10x                  |
| 60 to 69 | 20x                  |
| 70 to 79 | 40x                  |
| 80 to 89 | 60x                  |
| 90 to 99 | 80x                  |
| 100+     | 100x                 |

### Emissions Decay

Calculations are based on time passed, and rewards are updated every second.

Please note that emissions won't be generated for the first 30 minutes after sending a hero on an assignment/after claiming.

If a hero is left in an assignment for too long, his morale and productivity will be negatively impacted, affecting his total emissions. The hero's **total** emissions will drop to 70% after \~7 days (604,800 seconds), to 40% after \~14 days (1,209,600 seconds), and 10% after \~21 days (1,814,400 seconds).

You can reset the decay by claiming rewards without quitting the job or incurring any gas fees.

---
description: Stake your Heroes for $MEDALS and $ELM!
---

# Assignments

![Drop by the Adventurer's Guild to pick up assignments or chill for a drink.](<../../../.gitbook/assets/guild\_promo (1).jpg>)

## Working for MEDALS

Send idle heroes on assignments, which will award them MEDALS based on the different tasks. Assignments are an idle process for you. To send a hero on an assignment, you will have to choose an assignment, and choose the heroes eligible for it.

A hero's level will multiply his $MEDALS emissions in all jobs with fixed emissions. To see the emissions multiplier, check the [assignments ](assignments-fixed.md#emissions-multiplier)page.

####

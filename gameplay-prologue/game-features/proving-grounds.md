# Proving Grounds

<figure><img src="https://cdn-images-1.medium.com/max/880/1*dhQGsJUxETBxaARnCXLlJA.png" alt=""><figcaption><p>A New Challenge Awaits in Tales of Elleria</p></figcaption></figure>

The Proving Grounds stand as a testament to bravery, a place where heroes can prove their worth and claim treasures untold, bound to their soul's essence. As the seasons turn, the challenges within the tower shift and change, promising rewards for those brave enough to face them.

## Proving Grounds

Nestled atop the legendary Tower of Babel, The Proving Grounds offer a unique battleground where heroes from across the land can test their mettle, strategy, and courage.

<figure><img src="https://cdn-images-1.medium.com/max/880/1*s44MqibNNR_pcUuntc_IKA.png" alt=""><figcaption><p>Cerulean Drake — All Evolutions</p></figcaption></figure>

The Proving Grounds beckon with **fifteen escalating floors** of fierce battles, each more challenging than the last. The formidable Cerulean Drake serves as the gatekeeper, evolving its form **every five levels** to block your path to victory and rewards.

The challenge is laid out before you; it’s your move.

<figure><img src="https://cdn-images-1.medium.com/max/880/1*f3mRFncx5IYZPOTfGcUtxg.gif" alt=""><figcaption></figcaption></figure>

## Rotational Buffs and Bosses

Every two weeks, the Proving Grounds introduce new fearsome bosses — Crimson, Viridian, and Cerulean drakes, each demanding a different strategy to defeat. As our roster of bosses expands, expect the dynamics and duration of each season to evolve.

<figure><img src="https://cdn-images-1.medium.com/max/880/1*c8QZfO6622d5xuuwPuB-7g.png" alt=""><figcaption><p>Crimson, Viridian, and Cerulean drake</p></figcaption></figure>

This cycle, the Cerulean Drake takes center stage, wielding its fearsome ability to unleash **multi-hit attacks** and **reflect damage** onto its challengers. However, heroes are not left to face this threat unaided.&#x20;

Elysis, the goddess of Elleria, bestows her blessing upon all who enter the Proving Grounds, granting an additional **x2 Skill Defense from equipment infusions** to help counter the Cerulean Drake’s formidable assault.

{% hint style="info" %}
_Skill Defense reduces damage taken from monster's special attacks (e.g. Multihit)_

\
_You may find this glossary useful for an overview of different effects:_[#attributes-and-effects-glossary](../heroes/attributes/#attributes-and-effects-glossary "mention")
{% endhint %}

## Duration

The Proving Grounds will run biweekly, aligned with the Tower of Babel reset schedule, offering a fresh challenge with each cycle.

First season is starting on **26th February 2024**, after reset.

## Mechanics

* **Open to All:** Any hero can enter by paying an entry fee of 1,000 medals per attempt.
* **Unlimited Attempts:** Challenge the tower as many times as you wish.
* **Strategic Gameplay:** Plan your resources and equipment carefully to be the first few to conquer all 15 rounds.

## Rewards

* **Soul-Bound Equipment:** Earn T1-T3 equipment on the first clear of certain stages to significantly boost your hero’s capabilities.
* **Infusion Materials:** Earn infusion materials to enable more strategies in upcoming cycles and content.
* **Equipment Molds:** Earn equipment molds to craft new equipment.
* **$ELM Rewards:** The Proving grounds share the same $ELM reward pool as the Tower of Babel.&#x20;

## Leaderboard

Compete for glory and prizes on the leaderboard! Points are awarded based on the highest floor cleared, with tiebreakers in the respective order:&#x20;

1\. **Highest Floor Cleared** determines your rank.\
2\. **Higher Hero Level** takes precedence if floors are tied.\
3\. **Higher Hero Experience** (on clearance) tiebreaker if levels are also tied.\
4\. If all else is equal, the hero who **finished first ranks higher.**

{% hint style="warning" %}
Your leaderboard rank is set once you finish a level. Boosting your experience or hero level afterwards won't change your current rank. To improve your position, you'll need to complete the same level again or beat the next level.
{% endhint %}

Rewards are split between the Tower of Babel and Proving Grounds participants. The Tower of Babel’s reward pool is divided in half, with each portion equally distributed to the leaders of both challenges, ensuring fair and balanced rewards for all top competitors.

## Gems & Metals Conversion

<figure><img src="https://cdn-images-1.medium.com/max/880/1*nvJ1RZwuGe24oPr0nSwXWQ.png" alt=""><figcaption></figcaption></figure>

With the introduction of biweekly bosses and rotational buffs, we aim to enable players to craft the optimal infusion materials required to conquer these bosses.&#x20;

Gems and Metals can be converted to the **SAME TIER in a 3:1 ratio.**

## Equipment Salvage

<figure><img src="https://cdn-images-1.medium.com/max/880/1*BQ0lzR2PUZTiHSvFGHBdaw.png" alt=""><figcaption></figcaption></figure>

You can now **reroll two equipment pieces from the same set into one** by burning 2 pieces of equipment. The first selected piece of equipment will determine the equipment received after reroll. The base item has a symbol beside its selection.&#x20;

<figure><img src="https://cdn-images-1.medium.com/max/880/1*BNhJR9ap3thm-vLaza6CVw.png" alt=""><figcaption></figcaption></figure>

Please note that c**hoosing soul-bound equipment as a crafting material will result in the newly created equipment piece also being soul-bound**, regardless of whether a tradable piece was used alongside it.

<figure><img src="https://cdn-images-1.medium.com/max/880/1*HZy1Cr0q2GC_Qzy0g_eegw.png" alt=""><figcaption><p>Soulbound Warning</p></figcaption></figure>

The Proving Grounds are not just a test of strength but of intellect and resource management. This new feature encourages players to engage deeply with the game’s mechanics, strategize with their guilds, and share their triumphs and strategies with the community.

## Additional Notes

* **All quests will be automatically skippable on the Proving Grounds patch!**
* Adjusted medals from Ancient Ruins to match other regions.
* Upcoming world boss details will be announced soon.
* Ruby Infusion (DoT Resistance) balanced (1, 2, 5, 8, 10, 15% > 1, 2, 3, 4, 5, 7.5%)
* Gold Infusion (Dmg Reflect) balanced (1, 2, 4, 7.5, 12% > 0.5, 1, 1.5, 2.5, 4%)

We believe the Proving Grounds will become a cornerstone of Tales of Elleria, offering a space where legends are born, and tales of glory are forged. Whether you’re a seasoned veteran or a newcomer to our ranks, the Proving Grounds await your challenge.

Prepare your heroes, sharpen your blades, and may fortune favor the bold. Welcome to The Proving Grounds — where heroes prove their worth.\

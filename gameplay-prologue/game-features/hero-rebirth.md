# Hero Rebirth

The Hero Rebirth System allows you to burn one hero (Sacrificial hero) to reroll the stats of another (Main hero) at no extra cost. If you have heroes that you would like to try rerolling, the Hero Rebirth Feature is an option for you.

This feature will be released on 3rd April 2023 during reset (4 am UTC).

<figure><img src="https://cdn-images-1.medium.com/max/900/1*GU2WcJ2uh3Sz_L6sn8VotQ.png" alt=""><figcaption><p>Hero Rebirth UI</p></figcaption></figure>

## Mechanism

The main hero’s stats get re-generated between the shown attribute range without any weighted randomness. (same mechanism as mint)

After rebirth, your main hero undergoes the following changes:

* Stats get regenerated.
* Level and Daily Quest Attempts reset to 1.
* Accumulated EXP resets to 0.
* Soulbound stats are reset.
* Class remains the same.

{% hint style="warning" %}
The sacrificed hero will be burnt forever on-chain. They will show in your inactive tab as a soul.
{% endhint %}

<figure><img src="https://cdn-images-1.medium.com/max/900/1*e3Je4nBWq_MGbJIaoSxZlw.png" alt=""><figcaption></figcaption></figure>

## **All Hero Potential Bonus**

1. The minimum total attribute varies depending on the heroes used for rebirth:

<table><thead><tr><th width="210">Both Genesis Heroes</th><th width="186.33333333333331">Both Dawn Heroes</th><th>One Dawn &#x26; One Genesis Hero</th></tr></thead><tbody><tr><td>160</td><td>60</td><td>110</td></tr></tbody></table>

2. The maximum total attribute depends only on the main hero.\
   It will be 375 if the main hero is a Dawn Hero, and 400 if the main hero is a Genesis Hero.\\
3. $MAGIC shards can be used to improve the main stats range of your hero.\
   **For every 10 $MAGIC Shards used, the possible stat range used for rebirth increases by 1.** The maximum range of stat rollable using $MAGIC shards is capped between 376\~425.

```
Example:
ie no magic burnt = 2x genesis will roll for 160~400.
ie 1x10 magic burnt = 2x genesis will roll for 161~401.
.
.
.
ie 216x10 magic burnt = 2x genesis will roll for 376~425. (max)
```

## **Rebirth EXP Bonus**

To reward our most loyal players for the amount of time and effort that they have put into the game, EXP accumulated will give your heroes an advantage during rebirth.

The sum of involved heroes’ **excess EXP** will be used, and every 90 EXP will award the rebirthed hero 1 guaranteed extra stat point, up to a cap of 2700 EXP.

```
If your hero is level 1 at 50 EXP, the required EXP for next level is 0. 
All 50 EXP counts towards the bonus.

If your hero is level 10 at 50 EXP, the required EXP for next level is 5. 
EXP counted will be (50 — 5 = 45).
```

{% hint style="info" %}
EXP Bonus will be capped at 376 total attrbutes. If your hero rolls a number above that due to Magic Shard Bonus, EXP bonus will not apply further!
{% endhint %}

## Feature Guide

1. Head to the Town Square from the main menu and click on the Hero Rebirth feature.

<figure><img src="https://cdn-images-1.medium.com/max/900/1*7o3oDOOu_k0o8YDCNBTx-A.png" alt=""><figcaption></figcaption></figure>

2\. Choose one hero to be sacrificed (released and burnt) on the left and one hero to reroll on the right. The main hero’s class will remain the same!

<figure><img src="https://cdn-images-1.medium.com/max/900/1*XQ-NmxMbaWIP9WqjVqVI6w.png" alt=""><figcaption></figcaption></figure>

3\. (Optional) Use $MAGIC shards to improve the possible range of stats obtained after rebirth.

<figure><img src="https://cdn-images-1.medium.com/max/900/1*Wk8nHQlxQgsLTPSbnwAhHg.png" alt=""><figcaption></figcaption></figure>

4\. Follow the instructions to authorize the transactions and enjoy your new hero. Good luck!

<figure><img src="https://cdn-images-1.medium.com/max/900/1*DuP1wt-jKpsEC41lQDsHtw.png" alt=""><figcaption></figcaption></figure>

## **Total Final Attributes Formula**

```
Final Attributes:

[Minimum Total Attributes (1) + $MAGIC Shard Bonus (3) to
Maximum Total Attributes (2) + $MAGIC Shard Bonus (3)]
+ (Heroes EXP Bonus)


Mechanics behind the scene:

1. For all stats, generate between their min and max. 
Keep track of generated total attributes (generatedTotalAttributes).
2. Multiply each stat by (actualTotalAttributes/generatedTotalAttributes), cutting them off between min/max if necessary.
3. Re-calculate generatedTotalAttributes after capping. 
4. For the difference between generated and actual total attributes, add/minus 1 stat randomly.
```

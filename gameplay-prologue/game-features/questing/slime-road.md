---
description: >-
  Groups of slimes have been spotted roaming around the outer walls. Will you
  befriend or hunt them?
---

# Slime Road

<figure><img src="../../../.gitbook/assets/3 (1).png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
The Slime Road awards $MEDALS, Relics, ELM Shards, and EXP, allowing you to accumulate resources for heroes and towards obtaining skills.
{% endhint %}

### Possible Encounters:

{% tabs %}
{% tab title="Blue Slimes" %}
A little blue blob that can only headbutt passing travelers. Some of them are smaller and harmless, but the bigger ones can pack a nasty punch when they hit you in the wrong places!\
\
Deals magical damage and can be found almost everywhere. Their rewards differ depending on what they've consumed in their surroundings.
{% endtab %}

{% tab title="Plant Slime" %}
A green slime formed from flowers mutating. These slimes may appear cute, but they are shy and will retaliate when attacked or accidentally stepped on!\\

Deals damage by shooting magical projectiles formed within their bodies.
{% endtab %}

{% tab title="Lava Slime" %}
A reddish slime formed by mutated rocks pushed up from underground. These slimes are known to be more aggressive and will spit rocks at other moving creatures. There have been reports of fires due to the lava projectiles shot from these slimes.

Deals physical damage by ejecting rocks formed within their bodies.
{% endtab %}

{% tab title="Cactus Slime" %}
A slime formed by cacti mutating, and they are as dangerous as they look! These bulbous slimes shoot their spines in all directions when threatened, and have been spotted causing friendly fire as well.

Deals magical damage by releasing toxic spines formed within their bodies.
{% endtab %}

{% tab title="King Slime" %}
Zkdw pxwdwhg lqwr wklv? Wkhvh nlqjv kdyh ehhq vsrwwhg frppdqglqj rwkhu volphv, dqg duh nqrzq wr eh lqwhooljhqfh hqrxjk wr vwudwhjlch dqg zlhog pdqd. Wkh dgyhqwxuhu'v jxlg uhsruwv wkdw d uduh nlqg ri Volph Hvvhqfh fdq eh irxqg zlwklq wkhlu erglhv zkhq ghihdwhg.

Ghdov pdjlfdo gdpdjh eb uhohdvlqj klv volpb plqlrqv!
{% endtab %}
{% endtabs %}

### Uncovered Areas of Interest:

#### Slime Road - Slime Friend (Dawn Heroes Only)

These slimes are friendly and willing to spar a bit to get you experienced with combat! Your heroes will gain extra experience and be better ready for his journey!\
\
Preparation Fees: None!\
Heroes Required: 1\
Difficulty levels: 1

#### Slime Road - Slime Foe

Wandering further down the road, you see similar blue slimes that are aggressive. These slimes have consumed different pollutants and have built up some processed goo within them while also being slightly stronger.

Preparation Fees: 250 $MEDALS\
Heroes Required: 1\
Difficulty levels: 3

#### Slime Road - Slime Ambush

Our researchers have discovered that slimes grow stronger the more mana and ELM they consume. Slimes have started to ambush wanderers in an attempt to steal their possessions for consumption. Stop these slimes!!

Preparation Fees: 500 $MEDALS\
Heroes Required: 1\
Difficulty levels: 4

#### Slime Road - Slime Bandits

Some slimes that have consumed huge amounts of ELM have mutated into kings- and are organizing attacks on citizens and goblins alike. We need to stop these slimes from getting stronger-

Preparation Fees: 1000 $MEDALS\
Heroes Required: 1\
Difficulty levels: 4

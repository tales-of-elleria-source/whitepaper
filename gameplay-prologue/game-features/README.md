# Features Overview

![Goblins!](<../../.gitbook/assets/goblin\_reveal (1).png>)

### Heroes

Summon heroes to join you in your quest to fight against monsters for peace, fame, and wealth. Heroes will have unique classes and stats distribution, applicable in different situations.

Heroes are the entry point into TELL and are required for the other in-game features.

{% content-ref url="../heroes/" %}
[heroes](../heroes/)
{% endcontent-ref %}

### Assignments

Idle heroes can be sent on assignments for passive token emissions, allowing for a low-commitment way of being involved in Elleria.

Assignments are the primary method of $MEDALS emissions which allows you to pay in-game fees.

{% content-ref url="assignments/" %}
[assignments](assignments/)
{% endcontent-ref %}

### Solo Quests

Send your heroes on quests to defeat monsters for rewards, bounties, and equipment. Quest contents will differ based on area and seasons, requiring different expertise.

{% content-ref url="questing/" %}
[questing](questing/)
{% endcontent-ref %}

### Equipment

Obtain equipment from the blacksmith or quests to further strengthen your heroes and allow progression.

{% content-ref url="equipment.md" %}
[equipment.md](equipment.md)
{% endcontent-ref %}

### Wishing Well

Legends speak of a well that can grant your wishes. Toss items into the wishing well, and something good might happen.

### PVP System

There will be friendly contests and death tournaments with different reward and risk payouts accordingly. Champions of death tournaments will have a chance to choose between increased attributes, legendary relics or equipment, or riskless level upgrades.

### Team Quests

Assemble a team to venture on more difficult quests. These quests are more challenging and riskier than solo ones, requiring stronger characters and providing exponentially better rewards.

{% content-ref url="questing/" %}
[questing](questing/)
{% endcontent-ref %}

### Drops & Relics

{% content-ref url="../relics-drops-system/" %}
[relics-drops-system](../relics-drops-system/)
{% endcontent-ref %}

Drops and Relics can be obtained from different sources, such as from Wishing Well and Quests. Some of them are more precious than others. They can significantly enhance your team's or an individual's capabilities when used appropriately. Players will be able to craft consumables that can be used during Quests or craft key components or crafting materials used in more complex recipes.

### Shop System

{% content-ref url="barkeepers-shop.md" %}
[barkeepers-shop.md](barkeepers-shop.md)
{% endcontent-ref %}

The barkeeper has opened a shop where you can trade $ELM and other on-chain currencies for various relics useful within Elleria! Head to the Adventuerer's Guild to visit. Look out for some potentially limited items from our partner projects :eyes:

For projects looking to collaborate & integrate, please reach out to Smashe or Ellerian Prince in Discord.

### Daily/Weekly Tasks

{% content-ref url="tasks.md" %}
[tasks.md](tasks.md)
{% endcontent-ref %}

Elysis has decided to reward all dedicated citizens! Complete daily and weekly tasks for free elixirs, potions, $ELM, $MEDALS, and A FREE SCROLL.

### House System

Relics exist deep within monster territories to enhance your heroes' combat capabilities. A house can be built for your heroes with these relics as the core, to give benefits to your heroes.

### Land System

Once surrounding land has been cleared of monsters through quests, they will be available for conquest. Players will be able to build an outpost upon it to defend against monster raids and enable further world expansion.

### Guild System

Guilds will be established on newly claimed land and act as resting and resupply points. A framework will be established to allow players to take over system-made guilds, allowing players to take control.

### War System

Monsters have organized themselves to defend against us and attempt to attack our towns. There will be a town-defense and quota-based system to determine whether the world will continue expanding, or if monsters will take victory and the play area will regress.

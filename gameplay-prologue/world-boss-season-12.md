---
description: 🥷🏻 Zorag Alert! Calling all ninjas! Your skills are needed! 🥷🏻
---

# World Boss: Ninjas vs Zorag (Season 13)

_Zorag has returned to settle his unfinished business with the Ninjas! Sharpen your blades and practice your slashes against the mighty Zorag's battleaxe._

![Zorag, the Goblin King](<../.gitbook/assets/goblin\_camp\_ex\_withking (2).png>)

{% hint style="info" %}
Up to 6,125 $ELM in prizes are up for grabs for NINJAS ONLY! \
Read further to find out how to participate and score.
{% endhint %}

### Event Info <a href="#event-info" id="event-info"></a>

<figure><img src="../.gitbook/assets/image (2) (1).png" alt=""><figcaption><p>Head over to the City Gates > World Boss to find the Woodot Quest!</p></figcaption></figure>

Get ready to depart for this special World Boss event! You can access the event via [https://app.talesofelleria.com/](https://app.talesofelleria.com/) with a compatible web3 wallet and binded Ninjas.

***

### **Event Information**

**Duration**: 15th April 2024 to 29th April 2024 (4AM UTC). \
**Rewards**: 6,125 $ELM distributed across 1000 Ninjas. \
**Entry Criteria**: \
\- Ninjas only (NO TICKET COST)\
\- 1 Quest Attempt per entry (blue bar indicated on the hero card, resets 4AM UTC) \
\
**Scoring:** \
\- Highest damage dealt in a single attempt per ninja.

To learn more about obtaining a hero and starting, please refer to our [quickstart guide](https://docs.talesofelleria.com/welcome-to-elleria/guides/general-game-guide) or ask in our Discord! An overview is also available in the next section.

## Dive Deep into the World Boss Structure

<figure><img src="../.gitbook/assets/image (1) (1) (1).png" alt=""><figcaption><p>Obtain a Ninja via Treasure Marketplace!</p></figcaption></figure>

**Step 1: Obtaining a NInja**

Before you can challenge Zorag this time, you’ll need a Ninja by your side. If you don't already own a ninja, they're purchaseable from the [marketplace](https://app.treasure.lol/collection/tales-of-elleria?trait%5B%5D=Class%3ANinja). Gear up and prepare for the battle ahead!

<figure><img src="../.gitbook/assets/image (3) (1).png" alt=""><figcaption></figcaption></figure>

**Step 2: Engage in Battle**

In this boss fight, heroes will attempt to deal as much damage as possible before running out of HP. The **highest damage dealt per HERO in a single attempt** will be ranked on the leaderboard where prizes will be allocated.

A player can send multiple ninjas into the world boss. Wallets will receive rewards per ninja (you can receive multiple if you have multiple ninjas).

{% hint style="warning" %}
The leaderboard snapshot happens immediately before rewards distribution. \
\
Your hero must remain in your wallet until rewards are distributed or your attempt will be forfeit. You won't be affected if you don't move your heroes until after you receive the in-game mail!
{% endhint %}

**Battle Mechanics**

* The battle utilizes a turn-based system, and the boss can either use a special/normal attack per turn. Special attacks will hit for a minimum of 15% of the hero's HP.
* The boss will get empowered the longer the battle drags on. Beyond the 10th turn, the boss will have progressively increasing stats.
* Players can use one item per turn.
* A quest must be completed before the final cut-off time for its damage to count.
* A hero must remain in the player's wallet for its score to count. If the hero quests on a different account, all previous quest attempts are voided. (If the hero moves from A > B > A, all attempts from the original A before transfer are also voided)
* The Boss does Physical damage and will have fixed stats throughout the event.

## Ninja x Zorag World Boss Rewards <a href="#rewards-await-the-brave" id="rewards-await-the-brave"></a>

There will be 6,125 $ELM allocated up to the 1,000th Ninja, from the undistributed $ELM allocated to Ninjas via Tower of Babel.

The **$ELM prize pool allocation per event period** is as follows:

1st Place ► 245 $ELM\
2nd Place ► 183.75 $ELM\
3rd Place ► 122.50 $ELM\
4–10th Place ► 73.50 $ELM\
11–25th Place ► 49 $ELM\
26–50th Place ► 36.75 $ELM\
51–100th Place ► 29.40 $ELM \
101–200th Place ► 6.125 $ELM\
201–600th Place ► 2.45 $ELM\
601–1000th Place ► 0.8575 $ELM\

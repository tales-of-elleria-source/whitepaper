# Class Skills/Traits

## What are skills?

Skills are special actions performed in quests and used within the combat system. Different skills are useful in different scenarios, especially within team dungeons when multiple heroes can support each other mutually.

New classes released in the future will have skills unique to their roles, adding depth to our combat system by allowing for the affliction of different status effects, with each class having their unique traits.

Here is a table of the Heroes' skills. For more information, refer to the[ Combat System](../game-features/combat-system.md) section.

<table><thead><tr><th width="188.42118083638002">Class Name</th><th width="168.14136953931782">Skill Name</th><th>Skill Effect</th></tr></thead><tbody><tr><td>Warrior</td><td>Shield Bash</td><td>100% Accuracy, 55% Damage.<br>On hit, +30% DEF for 2 turns.</td></tr><tr><td>Assassin</td><td>Lethal Strike</td><td>100% Accuracy, either 40%/150% Damage.</td></tr><tr><td>Mage</td><td>Curse of Weakening</td><td>100% Accuracy, 30% Damage.<br>On hit, enemy -50% RES for 1-3 turns.</td></tr><tr><td>Ranger</td><td>Aimed Shot</td><td>100% Accuracy, 25% Damage.<br>The enemy gets 'Bleed' for the damage received from this attack for 2-4 turns.</td></tr><tr><td>Berserker</td><td>Berserk</td><td>100% Accuracy, Lower HP = Higher Damage.<br>(55% STR. DMG + 30% VIT. DMG) * [3 (0%) > %HP left > 0 (100%)].</td></tr><tr><td>Ninja</td><td>Infinity Blade</td><td>100% Accuracy, Build a +25% combo on consecutive uses against the same target (Max 250%)</td></tr><tr><td>Spiritualist</td><td>Devour</td><td>50% Accuracy, 200% damage.</td></tr><tr><td>Machinist</td><td>Future Shot</td><td>100% Accuracy, +20% Crit Rate for 2-3 turns</td></tr></tbody></table>

{% hint style="info" %}
The skill system is being revamped- Expect to unlock differing playstyles and viable strategies.
{% endhint %}

## What are traits?

Traits are currently predetermined and might be developed further after the other aspects of Elleria are mature. Traits affect the behaviours of classes on Quests. For example, when defending, different classes will react differently.

<table><thead><tr><th width="205.01762662891736">Class Name</th><th width="173.14136953931782">Trait</th><th>Defend Effect</th></tr></thead><tbody><tr><td>Warrior<br>Berserker<br>Spiritualist</td><td>Sturdy</td><td>+50% DEF for 2-4 turns.</td></tr><tr><td>Assassin<br>Ranger<br>Ninja</td><td>Nimble</td><td>+50% AGI for 2-4 turns.</td></tr><tr><td>Mage<br>Machinist</td><td>Spellcaster</td><td>+50% RES for 2-4 turns.</td></tr></tbody></table>

## Class Basic Attacks

Basic attacks are the staple of your battle. While every hero shares the same attacks now, there might be a way of upgrading or choosing different attacks in the future...

<table><thead><tr><th width="205.01762662891736">Class Name</th><th width="173.14136953931782">Attack Name</th><th>Effect</th></tr></thead><tbody><tr><td>Warrior</td><td>Slash</td><td>The hero bravely slashes his sword at the enemy.</td></tr><tr><td>Assassin</td><td>Stab</td><td>The hero nimbly stabs the enemy with a dagger.</td></tr><tr><td>Mage</td><td>Cast Spell</td><td>The hero casts an offensive spell that damages the enemy.</td></tr><tr><td>Ranger</td><td>Shoot</td><td>The hero lets loose an arrow that flies towards the enemy.</td></tr><tr><td>Berserker</td><td>Smash</td><td>The hero smashes his sword down onto the enemy.</td></tr><tr><td>Ninja</td><td>Slice</td><td>The hero draws his sword ands lices the enemy.</td></tr><tr><td>Spiritualist</td><td>Voodoo</td><td>The hero manipulates the dolls to attack the enemy.</td></tr><tr><td>Machinist</td><td>Bullet Wound</td><td>The hero shoots her trusty rifle at the enemy.</td></tr></tbody></table>

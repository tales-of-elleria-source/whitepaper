# Heroes

## Summoning Heroes

{% hint style="info" %}
The initial 10,000 Genesis Heroes have been minted out on 11 October 2022! The Genesis classes Warrior/Assassin/Mage/Ranger will most likely not be obtainable again.
{% endhint %}

**Dawn Heroes are in season! Find out more here:** [**https://medium.com/@talesofelleria/december-patchnotes-ef76272ddf81**](https://medium.com/@talesofelleria/december-patchnotes-ef76272ddf81)

Secondary Minting has commenced for Dawn Heroes utilizing an auction system requiring $USDC and $ELM. Each hero will cost 50 $USDC and between 0 $ELM - 100 $ELM.

250 heroes will be auctioned off weekly, starting with the Machinists.

* 1st Auction Period: 12th – 18th December 2022
* 2nd Auction Period: 19th – 25th December 2022
* 3rd Auction Period: 26th December 2022 – 1st January 2023
* 4th Auction Period: 2nd January 2023 – 8th January 2023

**At the end of each auction period, the 250 heroes will be allocated to the highest bidders. If there is a tie, it will be allocated to the fastest ones.**

<figure><img src="../../.gitbook/assets/image (8) (3).png" alt=""><figcaption></figcaption></figure>

**Notes**

1. Note that created bids cannot be refunded until the auction is over! After each auction, there is a time window (Monday - Tuesday) where bids can be withdrawn. Un-refunded bids will carry on to the next auction.
2. You will be able to increase your $ELM bid at any time while the auction is ongoing, from the 'My Active Bids' tab.
3. A snapshot will be taken at a random timing before or after the last reset to determine the highest bidders. **Do not wait until the last minute to place your bid!**

Heroes' attributes will be randomly generated (class and attributes) and stored on-chain.

#### Using Heroes In-Game (bridging)

To use Heroes in-game, users will first be required to bind heroes to their game wallet via a bridging smart contract.

**This will allow you to make transactions with our heroes in Elleria without incurring gas fees.**

A user can freely unbind a hero at any time to bring them back on-chain.

However, please take note that as unbinding a hero releases his soul from his physical body, he will lose all experience upon unbinding.

## Strengthening your Heroes

### Increasing Levels

Fresh heroes summoned from the altar will start at level 1. To increase a hero's level, you can pay certain amounts of Ellerium ($ELM) and Elleria Medals ($MEDAL) to the quartermaster to set up a trial and upgrade the hero's level. This level upgrade trial might be dangerous, and heroes will require experience gained through questing as a pre-requisite to level up for certain levels.

Refer to the table below for the fees, required experience, and failure probability.

Levels will NOT change your base attributes!

<table><thead><tr><th width="136">Level</th><th width="149">Medals /lvl</th><th width="128">Ellerium /lvl</th><th width="150">EXP /lvl</th><th width="150">Failure %</th></tr></thead><tbody><tr><td>1 > 9</td><td>2,000</td><td>0</td><td>0</td><td>0%</td></tr><tr><td>10 > 19</td><td>5,000</td><td>0</td><td>+5</td><td>0%</td></tr><tr><td>20 > 29</td><td>15,000</td><td>0</td><td>+10</td><td>0%</td></tr><tr><td>30 > 39</td><td>45,000</td><td>50</td><td>+30</td><td>10%</td></tr><tr><td>40 > 49</td><td>100,000</td><td>500</td><td>+55</td><td>20%</td></tr><tr><td>50 > 59</td><td>200,000</td><td>1,000</td><td>+100</td><td>25%</td></tr><tr><td>60 > 69</td><td>500,000</td><td>5,000</td><td>+100</td><td>30%</td></tr><tr><td>70 > 79</td><td>1,000,000</td><td>10,000</td><td>+100</td><td>35%</td></tr><tr><td>80 > 89</td><td>2,000,000</td><td>10,000</td><td>+400</td><td>40%</td></tr><tr><td>90 > 99</td><td>5,000,000</td><td>20,000</td><td>+200</td><td>50%</td></tr><tr><td>100 > 109</td><td>10,000,000</td><td>50,000</td><td>+200</td><td>60%</td></tr></tbody></table>

{% hint style="success" %}
Please refer to [this spreadsheet](https://docs.google.com/spreadsheets/u/1/d/e/2PACX-1vS00omPdlFwL8TXbziTMbnv9K-7o1uAwnl-Vr0JwOLtmnEwz0kMyXWzQRc0Dl8uRSc6h\_OnTiUk\_wkw/pubhtml?gid=0\&single=true) for a detailed breakdown of upgrade requirements!
{% endhint %}

Levels act as proof that a hero is experienced, giving them multiple benefits.

First, it allows them to qualify for better-paying jobs and exponentially increase their rewards from jobs. This emissions multiplier is specified in the table above.

Secondly, it increases the hero's combat attributes, making them stronger in dungeons to face more challenging enemies and bosses, providing exponentially better rewards.

Thirdly, it qualifies your hero for the leaderboards for the incentives awarded to top-tier heroes.

If you fail to upgrade, all fees used for the upgrade will be burnt. Your next upgrade will have an increased 5% chance of succeeding.

### Equipment

Equipment will NOT change your base attributes!

Your heroes can equip different items to improve their combat attributes. Refer to the [Equipment tab](../game-features/equipment.md) for more information.

### Increasing Basic Attributes

The world has ancient relics that can alter your hero's basic attributes, providing you with a chance to buff your characters. These artefacts can be crafted, obtained from boss monsters from certain quests, or unearthed from certain ancient ruins.

There might also be PVP tournaments planned that can increase the champion's stats...

These mechanics will allow any heroes to become stronger- Your heroes are not fixed upon minting and will have a chance to progress and grow stronger.

More information will be released as Elleria grows.

## Transferring Heroes

You will first have to unbind your heroes from the Summoning Altar to transfer them. When heroes are transferred, their EXP is reset to their level's required EXP. For example:

Level 20 = Max 60 EXP.\
Level 30 = Max 180 EXP.

We encourage using only the marketplace to sell your heroes: [https://trove.treasure.lol/collection/tales-of-elleria](https://trove.treasure.lol/collection/tales-of-elleria)

{% hint style="danger" %}
We advocate fair play. Any attempts to scam through OTC will not be tolerated and harsh actions will be taken against any attempted bad behaviour.
{% endhint %}

## Releasing Heroes

All heroes are valuable and shouldn't be disregarded. Should a player decide to leave the game, souls can be sold/transferred to other players through Trove Marketplace using $MAGIC.

EXP is reset to the lower limit of its level upon the release of your heroes.

Click on 'Refresh Metadata' on Trove to update your hero's stats after unbinding.

<figure><img src="../../.gitbook/assets/image (4) (3).png" alt=""><figcaption></figcaption></figure>

## Inactive Heroes

Heroes can become temporarily or permanently inactive. When they are inactive, they will be unable to work on Assignments/go on Quests. These heroes will show in the 'Inactive' tab in the heroes quarters.

When a hero is permanently inactive, they will revert into their soul forms and will be easily distinguishable in the marketplace and in-game.

<img src="../../.gitbook/assets/soul.gif" alt="" data-size="original">

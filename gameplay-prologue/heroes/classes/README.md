# Classes

A summoned hero will have a random class depending on the rates displayed on the altar. The initial classes include warriors, assassins, mages and rangers. More classes will be introduced in the future.

There are different rarities for classes: common, epic, and legendary. A class's rarity is determined by its maximum supply and summons chance %. This rarity only affects the chance of obtaining a specific class, and there is a separate rarity system for attributes.

### Warrior

A brave and noble fighter who dedicated his life to protecting Elleria. The Warrior is the perfect vessel for a journey of epic proportions. Warriors have the highest strength and vitality attributes. They can equip swords, shields and armour. The warrior does melee physical damage.

![Warrior (Epic)](<../../../.gitbook/assets/warrior\_t2\_promo (1).jpg>)

### Assassin

Assassins are silent but deadly attackers who are experts in daggers. Assassins have the highest agility attribute. Due to their agile nature, they have a high probability of avoiding damage. They can equip daggers, shields, and armours. Assassins deals melee physical damage.

![Assassin (Epic)](../../../.gitbook/assets/assassin\_t2\_promo.jpg)

### Mage

The Mage is a Maestro of magic and wielder of mana. They can inflict significant magical damage while debuffing the enemies. Mages have the highest intelligence and will attributes. They can equip staff and robes. Mages deal ranged magical damage.

![Mage (Epic)](<../../../.gitbook/assets/mage\_t2\_promo (1).jpg>)

### Ranger

As an expert of the bow, the Ranger can strike down her enemies with pinpoint accuracy. They can also lead the team away from traps. Rangers have high strength and agility attributes. They equip bows and armours. Rangers deal ranged physical damage.

![Ranger (Epic)](<../../../.gitbook/assets/archer\_t2\_promo (1).jpg>)

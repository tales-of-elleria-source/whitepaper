# Dawn Heroes

### **Berserker**

The Berserker is a ferocious and rugged brute who seeks the thrill of life through countless battles. He is a combat manic, and can withstand any attacks his foes can throw at him. Berserker wields dual axes, ready to mow down enemies with **physical damage**.

<figure><img src="../../../.gitbook/assets/image (4) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

### Ninja

Evildoers beware, the night is watching. The nimble Ninja blends into darkness and shadows to get where he needs to be, and his trusty katana slices all who dares to stand in his way. The ninja slices through enemies dealing **physical damage.**

<figure><img src="../../../.gitbook/assets/image (3) (1) (1) (1).png" alt=""><figcaption></figcaption></figure>

### Spiritualist

Mysterious, alluring and deadly. Holding a cursed voodoo doll, the Spiritualist conjures nether spirits to fight in her stead. The cursed damage inflicted by these spirits deals heavy **magical damage** and is said to defend well against physical attacks of the mortal realm.

<figure><img src="../../../.gitbook/assets/image (4) (4).png" alt=""><figcaption></figcaption></figure>

### **Machinist**

Armed with a deadly Ellerium-powered rifle, the machinist is ready to snipe down any goblins that stand in her way! This lethal sniper uses her intellect to deal **magical damage** while also requiring adequate agility to be proficient with her gun.

<figure><img src="../../../.gitbook/assets/image (6) (3).png" alt=""><figcaption></figcaption></figure>

# Battle Pass

<figure><img src="../.gitbook/assets/image.png" alt=""><figcaption></figcaption></figure>

## **Introduction**

The Tales of Elleria Battle Pass is a feature designed to enhance player experience by rewarding progress with exclusive items and boosts. It operates on a level-based system where players complete various missions to earn experience points (EXP), unlocking a plethora of rewards as they progress through different levels. This guide outlines the mechanics, benefits, and options available within the Battle Pass.



## **Structure and Progression**

**1. Earning Battle Pass XP:**

* Players can earn XP by completing a variety of missions, categorized into daily, weekly, and seasonal tasks. These missions are designed to suit all play styles and provide ample opportunity for progression.
* Each completed mission contributes XP towards the player's Battle Pass level.

<figure><img src="../.gitbook/assets/image (2).png" alt=""><figcaption></figcaption></figure>

**2. Level Advancement:**

* The Battle Pass contains numerous levels, each offering unique rewards. As players accumulate XP, they advance through these levels.
* Each level requires a set amount of XP to unlock, and the difficulty of missions and the XP rewards scale as players progress to higher levels.

<figure><img src="../.gitbook/assets/image (1).png" alt=""><figcaption></figcaption></figure>

## **Rewards System**

**1. Standard Rewards:**

All players participate in the Basic Tier of the Battle Pass, which grants rewards at every level without any additional cost.&#x20;

**2. Hero's Path Expansion:**

Players can choose to enhance their Battle Pass by purchasing the Hero's Path expansion for 50 magic shards. This upgrade provides access to premium rewards that are not available in the Basic Tier. Premium rewards include higher value items, exclusive skins, and significant gameplay enhancers.

**3. Dragonbane’s Treasure:**

For an immediate advancement, players can purchase the Dragonbane’s Treasure. This option allows players to skip 20 levels of the Battle Pass instantly, granting all the rewards from those levels immediately. This is particularly beneficial for players who start the Battle Pass later in the season or those looking to catch up quickly.

**4. Seasonal Rewards:**\
The first level reward features the exclusive Ares skin from the new Mythical Collection, setting a high standard for the quality and appeal of Battle Pass rewards. One class will be featured each battle pass cycle. \
\


## **Feature Details**

Each Battle Pass season lasts approximately one month. At the end of each season, the Battle Pass resets. Players begin the new season at level 1, allowing for renewed opportunities to earn rewards.

The Battle Pass is accessible directly from the Tasks button located at the bottom left of the game’s main interface. Players can view their current level, progress towards the next level, available missions, and rewards all in one centralized location.

<figure><img src="../.gitbook/assets/image (4).png" alt=""><figcaption></figcaption></figure>

## Full Battle Pass Rewards



<table><thead><tr><th width="91">Level</th><th>Adventurer's Journey</th><th>Hero's Path</th></tr></thead><tbody><tr><td>1</td><td>+30 Crafting EXP</td><td>1x Seasonal Costume</td></tr><tr><td>2</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>3</td><td>1000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>4</td><td>+30 Crafting EXP</td><td>2500 MEDALS</td></tr><tr><td>5</td><td>2x Diluted Recovery Potion (SB)</td><td>50x Ellerium Shards (SB)</td></tr><tr><td>6</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>7</td><td>1000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>8</td><td>+30 Crafting EXP</td><td>2500 MEDALS</td></tr><tr><td>9</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>10</td><td>1x Basic Charm Mold (SB)</td><td>1x Polished Charm Mold</td></tr><tr><td>11</td><td>+40 Crafting EXP</td><td>3000 MEDALS</td></tr><tr><td>12</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>13</td><td>1000 MEDALS</td><td>1x Polished Circlet Mold</td></tr><tr><td>14</td><td>+40 Crafting EXP</td><td>3000 MEDALS</td></tr><tr><td>15</td><td>2x Recovery Vial (SB)</td><td><strong>1x Polished Guards Mold</strong></td></tr><tr><td>16</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>17</td><td>2000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>18</td><td>+40 Crafting EXP</td><td>3000 MEDALS</td></tr><tr><td>19</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>20</td><td>1x Chaos Scroll (SB)</td><td>1x Lesser Positive Scroll</td></tr><tr><td>21</td><td>+50 Crafting EXP</td><td>3500 MEDALS</td></tr><tr><td>22</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>23</td><td>2000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>24</td><td>+50 Crafting EXP</td><td>3500 MEDALS</td></tr><tr><td>25</td><td>1x T1 Circlet EQ (SB)</td><td>1x T2 Circlet EQ</td></tr><tr><td>26</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>27</td><td>2000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>28</td><td>+60 Crafting EXP</td><td>4000 MEDALS</td></tr><tr><td>29</td><td>1x Fading Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>30</td><td>2x T2 Gem/Metal</td><td>2x T3 Gem/Metal</td></tr><tr><td>31</td><td>+60 Crafting EXP</td><td>4000 MEDALS</td></tr><tr><td>32</td><td>1x  Vivid Memories (SB)</td><td>1x Vivid Memories </td></tr><tr><td>33</td><td>2000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>34</td><td>+70 Crafting EXP</td><td>4500 MEDALS</td></tr><tr><td>35</td><td><strong>2x Random Elixir (SB)</strong></td><td>1x Jesters Tricks</td></tr><tr><td>36</td><td>1x  Vivid Memories (SB)</td><td>1x Timeless Memories</td></tr><tr><td>37</td><td>+70 Crafting EXP</td><td>4500 MEDALS</td></tr><tr><td>38</td><td>25x ELM Shard (SB)</td><td>1x Random Elixir</td></tr><tr><td>39</td><td>4000 MEDALS</td><td>1x Timeless Memories</td></tr><tr><td>40</td><td>1x Lesser Positive Scroll (SB)</td><td>1x Soul Wisp</td></tr><tr><td>41</td><td>+80 Crafting EXP</td><td>5000 MEDALS</td></tr><tr><td>42</td><td>25x ELM Shard (SB)</td><td>1x Timeless Memories</td></tr><tr><td>43</td><td>4000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>44</td><td>+80 Crafting EXP</td><td>5000 MEDALS</td></tr><tr><td>45</td><td>50x ELM Shard (SB)</td><td>1x Random Skin Lootbox</td></tr><tr><td>46</td><td>1x Random Elixir</td><td>1x Timeless Memories</td></tr><tr><td>47</td><td>5000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>48</td><td>+80 Crafting EXP</td><td>5000 MEDALS</td></tr><tr><td>49</td><td>6000 MEDALS</td><td>1x Random Elixir</td></tr><tr><td>50</td><td>1x Blessing Scroll (SB)</td><td>1x Blessing Scroll</td></tr></tbody></table>

# Artifacts/Consumables

As heroes make contact with the Ellerian Citizens, some craftsmen have reported having vivid dreams whereby an elusive figure guides them and bestows ancient knowledge.

Categorization and the study of artifacts/relics are still ongoing, and more information may be uncovered over time. These relics are grouped widely into Artifacts (usable outside battles) and Consumables (usable within battles).

## Artifacts

* Scrolls (these artifacts can alter a hero's stats)
* Revertables (can be used to revert the effect of certain scrolls)
* EXP Potions/Memories (to boost a hero's EXP)
* Skill-books (to obtain new skills)
* Lootboxes (to obtain a variety of resources)
* Soul Wisp (to rebirth heroes without burning a hero)

{% hint style="info" %}
Artifacts can be used from the Heroes Quarters under Manage Relics. Just click on the relic you want to use! Artifacts will be burnt upon usage.
{% endhint %}

### Scrolls & Revertables

![Scrolls](<../../.gitbook/assets/image (1) (1) (1) (1) (1).png>)

<table><thead><tr><th width="145">Id</th><th width="72">Tier</th><th width="237">Name</th><th>Effect</th></tr></thead><tbody><tr><td>30000</td><td>1</td><td>Risky Scroll</td><td>1 Random Stat:<br>30% -1<br>30% -2<br>30% -3<br>10% +10</td></tr><tr><td>30001</td><td>1</td><td>Cursed Scroll</td><td>1 Random Stat:<br>40% +10<br>60% -10</td></tr><tr><td>30002</td><td>3</td><td>Chaos Scroll</td><td>1-3 Random Stat:<br>5% -3<br>15% -2<br>20% -1 to +1<br>15% +2<br>5% +3</td></tr><tr><td>30003</td><td>5</td><td>Blessing Scroll</td><td>1-3 Random Stat:<br>30% +1<br>40% +2<br>30% +3</td></tr><tr><td>30004</td><td>5</td><td>Exalted Scroll</td><td>1 Random Stat:<br>40% +2<br>30% +3<br>20% +4<br>10% +5</td></tr><tr><td>30005</td><td>6</td><td>Miracle Scroll</td><td>3 Random Stat:<br>10% +1<br>20% +2<br>40% +3<br>20% +4<br>10% +5</td></tr><tr><td>30009</td><td>2</td><td>Lesser Chaos Scroll</td><td>1 Random Stat:<br>33.3% -1 to +1</td></tr><tr><td>30010</td><td>4</td><td>Lesser Positive Scroll</td><td>1 Random Stat:<br>100% +1</td></tr><tr><td>20010</td><td>2</td><td>Jester's Tricks</td><td>Reverts T3+ Scrolls (must use immediately after scroll is burnt)</td></tr><tr><td>20011</td><td>4</td><td>Plague Doctor's Secrets</td><td>Reverts T1+ Scrolls (must use immediately after scroll is burnt)</td></tr></tbody></table>

Using soul-bound scrolls will add to the hero's [soul-bound stats](../heroes/attributes/soul-bound-attributes.md), represented in brackets.

**FAQ:**

1. Maxed-out stats (including stats maxed from soul-bound scrolls) will not be included for randomization for scrolls Tier 4+. However, if you have an 89/90 and the scroll's minimum is +2, you will only get +1. See point 2.
2. If a stat has not been maxed out, it can be rolled multiple times (ie using a miracle scroll when a stat is at 89/90, which can result in an 89+15/90 which ultimately is only 90/90 (+1).
3. If a scroll has 3 rolls and your hero has 4 maxed stats, the scroll cannot be used as you have insufficient non-maxed stats.
4. Reverting will not refund the scroll.
5. The same stat can be rolled multiple times.
6. Stats will stick to the hero permanently, except for untradeable scrolls where stats will be reverted on transfer. These stats will show up in a bracket (+X) or (-X).
7. If a stat is at its minimum (30 for Genesis heroes and 10 for Dawn heroes), all negative rolls from tier 1-3 scrolls will not reduce its stat points further and result in a (0).

<details>

<summary><strong>Roll Mechanics:</strong></summary>

```
1. Eligible stats for rolls will be chosen:
If the scroll is under tier 4, all stats will be eligible.
If the scroll is tier 4 and above, only non-maxed stats will be eligible.
(Stats maxed from soul-bound scrolls are considered maxed)

2. You can only roll if the number of eligible stats is equivalent or greater than
the number of rolls the scroll has.

3. For the number of rolls the scroll has: 
   - Choose an eligible stat
   - Roll a random number based on the scroll %s.
   - Add that stat to a tracker to account for total changes.
   
4. After all rolls, the total change will be applied to the hero. 
Any min/maxed stats out of range will be capped.

If using a permanent scroll and existing soul-bound stats cause the hero's stats to
exceed the max or fall below the minimum, the soul-bound stats will be overwritten.
```

</details>

### EXP Potions

<figure><img src="../../.gitbook/assets/image (24).png" alt="" width="537"><figcaption><p>EXP Potions/Memories</p></figcaption></figure>

<table><thead><tr><th width="150">Id</th><th>Name</th><th>Effect</th></tr></thead><tbody><tr><td>30032</td><td>Fading Memories</td><td>+10 Hero EXP</td></tr><tr><td>30033</td><td>Vivid Memories</td><td>+50 Hero EXP</td></tr><tr><td>30034</td><td>Timeless Memories</td><td>+100 Hero EXP</td></tr></tbody></table>

### Soul Wisp

<figure><img src="../../.gitbook/assets/image (26).png" alt="" width="151"><figcaption><p>Soul Wisp</p></figcaption></figure>

<table><thead><tr><th width="150">Id</th><th>Name</th><th>Effect</th></tr></thead><tbody><tr><td>30014</td><td>Soul Wisp</td><td>Allows for Hero Rebirth</td></tr></tbody></table>

Soul Wisps can be used to Rebirth an existing hero without burning a sacrificial one. They will behave like a Genesis Hero with 0 EXP when determining the rebirthed hero's stats.

## Consumables

* Potions (a concoction of HP-boosting materials, these potions can be used in combat to recover lost health)
* Elixirs (a concoction of different boosting materials, these potions grant can be used in combat to grant temporary powers)

{% hint style="info" %}
You can use 1 consumable per hero per turn in quests, by selecting the 'Items' button. Consumables will be burnt upon usage!
{% endhint %}

### Potions

![Potions](<../../.gitbook/assets/image (5) (1) (1) (1).png>)

<table><thead><tr><th width="150">Id</th><th>Name</th><th>Effect</th></tr></thead><tbody><tr><td>40000</td><td>Diluted Recovery Potion</td><td>Recovers 10% HP</td></tr><tr><td>40001</td><td>Recovery Potion</td><td>Recovers 20% HP</td></tr><tr><td>40002</td><td>Potent Recovery Potion</td><td>Recovers 40% HP</td></tr><tr><td>40003</td><td>Superior Recovery Potion</td><td>Recovers 80% HP</td></tr><tr><td>40022</td><td>Diluted Recovery Vial</td><td>Recovers 100 HP</td></tr><tr><td>40023</td><td>Recovery Vial</td><td>Recovers 200 HP</td></tr><tr><td>40024</td><td>Potent Recovery Vial</td><td>Recovers 500 HP</td></tr><tr><td>40025</td><td>Superior Recovery Vial</td><td>Recovers 1000 HP</td></tr></tbody></table>

<figure><img src="../../.gitbook/assets/image (25).png" alt="" width="563"><figcaption><p>Elixirs</p></figcaption></figure>

<table><thead><tr><th width="194">Id</th><th width="244.80739502294057">Name</th><th>Effect</th></tr></thead><tbody><tr><td>40004</td><td>Warrior's Elixir</td><td>+ 25% Max HP and 25% Defense for 10 turns</td></tr><tr><td>40005</td><td>Assassin's Elixir</td><td>+ 25% Crit Damage and 25% Crit Resistance for 10 turns</td></tr><tr><td>40006</td><td>Mage's Elixir</td><td>+ 25% Final Attack and 25% Resistance for 10 turns</td></tr><tr><td>40007</td><td>Ranger's Elixir</td><td>+ 25% Max HP and 25% Crit Damage for 10 turns</td></tr><tr><td>40008</td><td>Luck Elixir</td><td>+ 10% Crit Rate for 10 turns</td></tr></tbody></table>

{% hint style="warning" %}
All elixirr boosts stack except Max HP, which uses the highest % boost available. If you have a 25% and 30% boost applied together -> Your hero will have 30% increased Max HP throughout the quest.
{% endhint %}

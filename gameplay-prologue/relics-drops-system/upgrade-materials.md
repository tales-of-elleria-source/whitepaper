# Upgrade Materials

Alchemists and blacksmiths have experimented with Ellerium and concluded that it can be used as a catalyst to allow for the transmutation and fusion of certain metals and gems. Infusing these into equipment will give them a bonus effect that will be applied to the Hero.

The higher tier the relic infused, the stronger the enhancement!\
See the Infusion Effects page for a full list of possible effects.

{% content-ref url="../equipment-system/infusion-effects.md" %}
[infusion-effects.md](../equipment-system/infusion-effects.md)
{% endcontent-ref %}

## Metals

<figure><img src="../../.gitbook/assets/image (29).png" alt=""><figcaption><p>Gold</p></figcaption></figure>

Every Metal has 5 different tiers, from 1 to 5. Lower-tiered metals can be combined and compressed into higher-tiered metals through the Relic Crafting System.

<table><thead><tr><th width="147">Metal Tiers</th><th width="159.33333333333331">Type</th><th>Amount required to merge (rank up)</th></tr></thead><tbody><tr><td>1</td><td>Powder</td><td>10</td></tr><tr><td>2</td><td>Ore</td><td>10</td></tr><tr><td>3</td><td>Tablet</td><td>10</td></tr><tr><td>4</td><td>Ingot</td><td>10</td></tr><tr><td>5</td><td>Star</td><td>10</td></tr></tbody></table>

## Gems

![Ruby](<../../.gitbook/assets/image (20) (1) (1).png>)

Every Gem has 6 different tiers, from 1 to 6. Lower-tiered gems can be combined and compressed into higher-tiered gems through the Relic Crafting System.

<table><thead><tr><th width="147">Gem Tiers</th><th width="159.33333333333331">Type</th><th>Amount required to merge (rank up)</th></tr></thead><tbody><tr><td>1</td><td>Chip</td><td>5</td></tr><tr><td>2</td><td>Shard</td><td>5</td></tr><tr><td>3</td><td>Plate</td><td>5</td></tr><tr><td>4</td><td>Jewel</td><td>5</td></tr><tr><td>5</td><td>Gemstone</td><td>5</td></tr><tr><td>6</td><td>World</td><td>5</td></tr></tbody></table>

---
description: >-
  Upgrade Chances % and $MEDALS cost the same for all equipment pieces- $ELM and
  protected $ELM cost differs per tier.
---

# Upgrade Costs

## Tier 1

<table><thead><tr><th width="153">★ Level</th><th width="196">Cost ($MEDALS)</th><th width="184">Cost ($ELM)</th><th>Protect Cost ($ELM)</th></tr></thead><tbody><tr><td>0 > 1</td><td>1,000</td><td>0</td><td>0</td></tr><tr><td>1 > 2</td><td>2,000</td><td>0</td><td>0</td></tr><tr><td>2 > 3</td><td>3,000</td><td>0</td><td>0</td></tr><tr><td>3 > 4</td><td>4,000</td><td>0</td><td>0</td></tr><tr><td>4 > 5</td><td>5,000</td><td>0</td><td>0</td></tr><tr><td>5 > 6</td><td>10,000</td><td>1</td><td>0</td></tr><tr><td>6 > 7</td><td>15,000</td><td>2</td><td>0</td></tr><tr><td>7 > 8</td><td>20,000</td><td>3</td><td>0</td></tr><tr><td>8 > 9</td><td>25,000</td><td>4</td><td>10</td></tr><tr><td>9 > 10</td><td>30,000</td><td>5</td><td>20</td></tr><tr><td>10 > 11</td><td>40,000</td><td>20</td><td>0</td></tr><tr><td>11 > 12</td><td>50,000</td><td>40</td><td>50</td></tr><tr><td>12 > 13</td><td>60,000</td><td>60</td><td>100</td></tr><tr><td>13 > 14</td><td>70,000</td><td>100</td><td>250</td></tr><tr><td>14 > 15</td><td>80,000</td><td>250</td><td>500</td></tr></tbody></table>

## Tier 2

<table><thead><tr><th width="153">★ Level</th><th width="196">Cost ($MEDALS)</th><th width="184">Cost ($ELM)</th><th>Protect Cost ($ELM)</th></tr></thead><tbody><tr><td>0 > 1</td><td>1,000</td><td>0</td><td>0</td></tr><tr><td>1 > 2</td><td>2,000</td><td>0</td><td>0</td></tr><tr><td>2 > 3</td><td>3,000</td><td>0</td><td>0</td></tr><tr><td>3 > 4</td><td>4,000</td><td>0</td><td>0</td></tr><tr><td>4 > 5</td><td>5,000</td><td>0</td><td>0</td></tr><tr><td>5 > 6</td><td>10,000</td><td>2</td><td>0</td></tr><tr><td>6 > 7</td><td>15,000</td><td>4</td><td>0</td></tr><tr><td>7 > 8</td><td>20,000</td><td>6</td><td>0</td></tr><tr><td>8 > 9</td><td>25,000</td><td>8</td><td>20</td></tr><tr><td>9 > 10</td><td>30,000</td><td>10</td><td>40</td></tr><tr><td>10 > 11</td><td>40,000</td><td>40</td><td>0</td></tr><tr><td>11 > 12</td><td>50,000</td><td>80</td><td>100</td></tr><tr><td>12 > 13</td><td>60,000</td><td>120</td><td>200</td></tr><tr><td>13 > 14</td><td>70,000</td><td>200</td><td>500</td></tr><tr><td>14 > 15</td><td>80,000</td><td>500</td><td>1,000</td></tr></tbody></table>

## Tier 3

<table><thead><tr><th width="148">★ Level</th><th width="196">Cost ($MEDALS)</th><th width="184">Cost ($ELM)</th><th>Protect Cost ($ELM)</th></tr></thead><tbody><tr><td>0 > 1</td><td>1,000</td><td>0</td><td>0</td></tr><tr><td>1 > 2</td><td>2,000</td><td>0</td><td>0</td></tr><tr><td>2 > 3</td><td>3,000</td><td>0</td><td>0</td></tr><tr><td>3 > 4</td><td>4,000</td><td>0</td><td>0</td></tr><tr><td>4 > 5</td><td>5,000</td><td>0</td><td>0</td></tr><tr><td>5 > 6</td><td>10,000</td><td>3</td><td>0</td></tr><tr><td>6 > 7</td><td>15,000</td><td>6</td><td>0</td></tr><tr><td>7 > 8</td><td>20,000</td><td>9</td><td>0</td></tr><tr><td>8 > 9</td><td>25,000</td><td>12</td><td>20</td></tr><tr><td>9 > 10</td><td>30,000</td><td>15</td><td>40</td></tr><tr><td>10 > 11</td><td>40,000</td><td>60</td><td>0</td></tr><tr><td>11 > 12</td><td>50,000</td><td>120</td><td>100</td></tr><tr><td>12 > 13</td><td>60,000</td><td>180</td><td>200</td></tr><tr><td>13 > 14</td><td>70,000</td><td>300</td><td>500</td></tr><tr><td>14 > 15</td><td>80,000</td><td>750</td><td>1,000</td></tr></tbody></table>

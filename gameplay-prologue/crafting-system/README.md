# Crafting System

![](<../../.gitbook/assets/image (9) (1) (2).png>)

Citizens can make use of the Blacksmith's dismantling/crafting services, as long as you provide the materials and required funds.

* Some items must be purchased from the shopkeeper.
* Some items are only dropped by monsters.
* Some items are only obtainable through the wishing well.
* Every recipe has its own costs and success rates, shown in the UI and in the recipe book.
* A list of the blacksmith's known recipes will be published in the Whitepaper.

### Crafting Mechanics - EXP & Slots

You can use up to 6 different unique materials when crafting. The amount of slots you have is dependent on the accumulated EXP of all your heroes.

Most basic-intermediate items can be crafted with 3 slots.

Specific advanced items will require 4 slots and above, and those are to faciliate end-game content and to allow crafters an edge by providing opportunities, or a 'dedicated crafter' in guilds.

{% hint style="info" %}
Crafting has its own EXP system. When you craft/disassemble/convert any item, you gain Crafting EXP equivalent to the ELM Shards cost.\
\
(Doesn't include Shard cost in recipe, etc. Lesser Positive Scroll gives 85 Crafting EXP)
{% endhint %}

The higher your crafting mastery, the more skilled you are at extracting $ELM Shards from monsters.

| Total Crafting Mastery (EXP) | Crafting Slots Unlocked | Bonus ELM Shards |
| ---------------------------- | ----------------------- | ---------------- |
| 0 - 299                      | 3                       | 0%               |
| 300 - 999                    | 4                       | 2.5%             |
| 1,000 - 1,999                | 5                       | 5%               |
| 2,000 - 9,999                | 6                       | 7.5%             |
| 10,000 and greater           | 6                       | 10%              |

* In each slot, you can specify the amount of a specific material to include.
* You will not be allowed to craft if the recipe is unknown.
* A random portion of your base materials will be lost if the crafting fails.
* Crafting is a 100% success now, in the future you can protect your materials using ELM Shards.
* The blacksmith will require ELM Shards as a catalyst to work with your items.
* The materials used for crafting must be bridged within the game.

## How to Craft?

1. Identify a recipe from the recipes section.
2. Place one set of the materials required (you can select how many sets to craft later).
3. Click on the 'Craft' button that should be lighted up now!
4. Select the quantity to craft, click on 'craft' again and sign the message to confirm!

![](<../../.gitbook/assets/image (10) (1) (1).png>)

## Recipes

In Elleria, there are 3 different categories of crafting. They are:

1. Dismantling
2. Conversion
3. Crafting

Please refer to the following tabs for more information

{% content-ref url="dismantling.md" %}
[dismantling.md](dismantling.md)
{% endcontent-ref %}

{% content-ref url="conversion.md" %}
[conversion.md](conversion.md)
{% endcontent-ref %}

{% content-ref url="crafting.md" %}
[crafting.md](crafting.md)
{% endcontent-ref %}

# Conversion Recipes

## Recipes

{% hint style="warning" %}
Recipe changes are subjected to change due to game balancing over time
{% endhint %}

<table><thead><tr><th width="252.1461551168489">Required Materials</th><th width="282.7439485400122">Outcome</th><th>ELM Shards Cost</th></tr></thead><tbody><tr><td>1x Goblin Trainee Doll<br>1x Ellerium Shard</td><td>5x Small Goblin Insignia<br>1x T1 Gem</td><td>30</td></tr><tr><td>1x Goblin Warrior Doll<br>1x Ellerium Shard</td><td>7x Small Goblin Insignia<br>1x T1 Gem</td><td>30</td></tr><tr><td>1x Goblin Mage Doll<br>1x Ellerium Shard</td><td>5x Big Goblin Insignia<br>1x T1 Gem</td><td>30</td></tr><tr><td>1x Hopgoblin Doll<br>1x Ellerium Shard</td><td>6x Big Goblin Insignia<br>1x T1 Gem</td><td>30</td></tr><tr><td>1x Goblin Leader Doll<br>1x Ellerium Shard</td><td>12x Big Goblin Insignia<br>1x T2 Gem</td><td>30</td></tr><tr><td>1x Weird Parchment</td><td>1x Cloth Strips</td><td>3</td></tr><tr><td>5x Three-leaf Clover<br>10x Ellerium Shard</td><td>1x Four-leafed Clover</td><td>0</td></tr></tbody></table>

##

# NFT/$ELM Bridging

## Blockchain & Game Synergy

Tales of Elleria will be expanding into a real-time open world. We have to create an off-chain server to manage in-game databases and allow for real-time transactions and synchronization that doesn't require gas.

Even during the prologue, our core gameplay loop requires a user to make multiple transactions- to go on assignments, claim rewards, stop an assignment, go on quests, upgrade heroes and equipment, reroll equipment, etc.

To mitigate the cost of transactions, apart from the initial on-chain minting and swapping of tokens on DEXs, everything else will be done in-game. This brings about some pros and cons, which we will cover here.

### Pros

* Gas fees decreased by almost 95%; Also reducing congestion on the Blockchain itself.
* Critical parameters such as NFT attributes are still stored on-chain to prevent manipulation. The game server and Blockchain integrate smoothly to make this happen.
* Allows for a seamless experience where users can play without worrying about transaction costs and allow for real-time battle mechanics.
* Equipment and other items can be farmed, upgraded in-game, and then claimed (minted) to trade. Doing so allows someone to turn their efforts and time into brand new NFTs that will have value.

### Cons

* Lack of transparency in the off-chain codebase. Mitigation: All formulas are published in the whitepaper and can be verified. All smart contracts will be verified too.
* Additional dependency on game server. Mitigation: We have an experienced full stack developer that can handle scalability and ensure server uptime.

Ultimately, the pros outweigh the cons. it is worth developing in this direction to allow for the complete RPG experience in the future.

## How to Bridge Heroes/Relics

Summoned heroes exist as souls, and must be bridged into the game to use them. The term to use for bridging into the game = 'Binding' aka 'Binding Heroes', and for bridging out = 'Release' aka 'Releasing Heroes'.

To Bind/Release heroes:

1. Log into the game and head to the 'Summoning Altar' from the world map.

![](<../../.gitbook/assets/image (3) (1) (1) (1) (1).png>)

2\. Click on 'Manage Souls' for Heroes, or 'Manage Relics' for Relics, followed by either Bind or Release.

<figure><img src="../../.gitbook/assets/image (1) (4).png" alt=""><figcaption></figcaption></figure>

3\. On PC, drag the NFTs to the box on the left. On Mobile, just tap on the NFTs.\
Once the NFTs you want to bridge are in the box, click on the 'Bind/Release' button.

![](<../../.gitbook/assets/image (13) (1) (1).png>)

Confirm the transactions, and you are done with bridging your NFTs!

{% hint style="info" %}
If you cancel a relic claim halfway, your Relics will be stuck temporarily. Check the !lostrelics command in Discord to learn how to continue the unbinding. (There is a button you can use from the top left nav menu)
{% endhint %}

## How to Bridge $ELM

ELM must be bridged into the game to use them without gas fees. The term to use for bridging ELM into the game = 'Deposit/Topping Up' aka 'Moving ELM into the game', and for bridging out = 'Claim/Cashing Out' aka 'Moving ELM out of the game'.

To Claim/Deposit $ELM:

1. Log into the game look at your profile card in the world map. There is a toggle on the top left. Clicking this toggle will alternate between your in-game and on-chain balances.

If you want to claim, just click on the claim button, as shown in the screenshot.

<figure><img src="../../.gitbook/assets/image (10) (2).png" alt=""><figcaption></figcaption></figure>

To deposit, click on the toggle once to show the Deposit button, as shown in the screenshot.

<figure><img src="../../.gitbook/assets/image (5) (4).png" alt=""><figcaption></figcaption></figure>

2\. Fill in the prompt and click Deposit/Claim accordingly, and confirm any transactions/signing messages.

<figure><img src="../../.gitbook/assets/image (7) (1).png" alt=""><figcaption></figcaption></figure>

{% hint style="warning" %}
If you cancel a claim transaction halfway, your $ELM will be stuck temporarily. Check the !lostelm command in Discord to learn how to continue the claim. (There is a button you can use from the top left nav menu)
{% endhint %}

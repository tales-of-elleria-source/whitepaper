# Player Types

There are different ways to be involved in TELL. We have categorised some of them here to allow players to understand how they might want to be involved depending on their commitment levels.

### Low Commitment

* **Stakers:** Players who have little time or do not learn about quests. These players can mint or buy heroes, send them on assignments, and periodically claim $MEDALS directly.
* **Traders:** Players who do not play the game but trade our ERC20 or ERC721 tokens. Traders need to understand how the game works and the importance of the tokens to trade well.
* **Minters:** Players who have good luck or high capital and want to use it. These players only mint new heroes to sell off during summoning events, which might be worth 10x-100x+ on their mint prices.

### Medium Commitment

* **Small-time Adventurers:** Players who upgraded and have decent heroes without equipment. These players go on low-levelled quests, clearing weaker monsters around the town for rewards.
* **Blacksmiths:** Players who have researched the equipment system and focus only on minting, trading, and upgrading equipment for profits. These players contribute to the ecosystem by reselling good equipment at high prices to give them the value they deserve.
* **Coliseum Dwellers:** Players who seek the thrill and rewards from fighting against other heroes and players to climbing the leaderboards, or players who wager on tournament matches for winnings. These players contribute to the competitive scene within the game.

### High Commitment

* **Veteran Adventurers:** Players who have upgraded and have a team of strong heroes equipped with proper equipment. These players can go on higher-levelled quests, risking their lives to slay bosses and conquer the unknown for rare and bountiful rewards.
* **Metaverse Contributors**: Players who contribute to the game by building tools and infographic websites or creating derivatives. If you are one of these, do reach out to the team and we will notice you.
* **Guild Coordinators:** Players who contribute to the game by managing communities, collating and sharing research information, translating announcements and content. If you are one of these, do reach out to the team too! We appreciate your help in expanding our community.

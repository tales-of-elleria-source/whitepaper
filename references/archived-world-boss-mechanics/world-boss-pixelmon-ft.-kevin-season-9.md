# Kevin World Boss

_The realm of Elleria is about to experience a ripple in its world as we announce an epic collaboration with Pixelmon. The serene lands will now echo the roar of the most anticipated world boss — Kevin!_

<figure><img src="https://cdn-images-1.medium.com/max/800/1*H3VbsXiC1n1cGQurx_9PFQ.png" alt=""><figcaption></figcaption></figure>

## A New Chapter Begins in Elleria

Elleria has always envisioned collaborations that not only amplify the gaming experience but also knit the virtual worlds closer. With Pixelmon, we have found the ideal synergy. Both games are built upon the pillars of dedication, sustainability, and, most importantly, community.

The introduction of Kevin, a beloved character from the Pixelmon community, marks a novel cross-IP development. Our teams have combined creativity and technology, resulting in a 3D reincarnation of Kevin, complete with unique animations and effects.

<figure><img src="https://cdn-images-1.medium.com/max/800/1*c7M5pb7zGdJMtCS2CD4X5Q.gif" alt=""><figcaption></figcaption></figure>

## Event Duration

This monumental event is set to launch on **28 August 2023 at 4 am UTC,** spanning two weeks and concluding on **10 September 2023 at 4 am UTC.**

## Dive Deep into the World Boss Structure

### **Step 1: Obtaining a Hero**

<figure><img src="https://cdn-images-1.medium.com/max/800/1*XPM9Ivfuh-O9wvpexulvtw.png" alt=""><figcaption></figcaption></figure>

Before you can challenge Kevin, you’ll need a hero by your side. For newcomers to Tales of Elleria, you can obtain a [free hero](https://medium.com/@talesofelleria/ethereal-heroes-a-new-era-5eef5b2b9c82) through the summoning altar, or purchase a tradable hero from the [trove marketplace](https://app.treasure.lol/collection/tales-of-elleria). Assemble your team and prepare them for the grand battle that awaits!

### **Step 2: Securing Entry Tickets**

<figure><img src="https://cdn-images-1.medium.com/max/800/0*glo9govE5sO1jvAE" alt=""><figcaption></figcaption></figure>

You will require an entry ticket in order to challenge Kevin. Equip yourself with an entry ticket, either **crafted from quest item drops**, **purchased from trove marketplace, or procured from the adventurer’s shop**.

<figure><img src="https://cdn-images-1.medium.com/max/800/0*-qHzcPl10s6M7ecP" alt=""><figcaption></figcaption></figure>

> Hint: It is cheaper to [craft your entry tickets](https://docs.talesofelleria.com/gameplay-prologue/crafting-system/crafting#consumable-recipes) than purchase them from Trove marketplace or from the adventurer’s shop!

### **Step 3: Engage in Battle**

In this boss fight, heroes will attempt to deal as much damage as possible before perishing to the world boss. The **total amount of damage done towards Kevin across all heroes and attempts** will be ranked on the leaderboard where prizes will be allocated.

A player can send an unlimited number of heroes into the world boss if he has sufficient entry tickets and quest attempts.

#### **Battle Mechanics**

* There’ll be a set pattern for the first ten turns but subsequently randomized between normal & special attack at a specific chance.
* Kevin’s normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Kevin will be. Beyond ten turns, Kevin enters into rage mode and will have progressively increasing stats.
* You can use one potion or elixir per turn, the same as in normal quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated.
* Kevin does phyiscal damage and has fixed stats.

<figure><img src="https://cdn-images-1.medium.com/max/800/1*IysTeMrxqW6ob-L37TDJuw.gif" alt=""><figcaption></figcaption></figure>

## Rewards Await the Brave

A substantial reward pool of **10,000 $MAGIC + 0.25 $MAGIC** per entry, an **Unhatched Pixelmon Egg (0.6 ETH)**, and a **Pixelmon Trainer (0.08 ETH)** will be distributed.

1st Place ► 5% of the pool + Plague Doctor’s Secret \
2nd Place ► 4% of the pool + Plague Doctor’s Secret \
3rd Place ► 3% of the pool + Plague Doctor’s Secret \
4–10 Place ► 2% of the pool + Jester’s Tricks \
11–25th Place ► 1.5% of the pool + Chaos Scroll \
26–50th Place ► 1% of the pool + Random Elixir \
51–100th Place ► 0.3% of the pool + Recovery Potion \
101–200th Place ► 0.1% of the pool + Random Goblin Part \
Special Reward ► 1.5% of the pool to the Player who attempted the world boss the most number of times. \
Participation reward ► 5,000 MEDALS.

Furthermore, every attempt at the world boss rewards players with a special **Proof of Kevin Trophy**, a soulbound relic signifying their valor.

<figure><img src="https://cdn-images-1.medium.com/max/800/1*DP50hHPHZh_fsD6jAN-ZAg.png" alt=""><figcaption><p>Proof of Kevin</p></figcaption></figure>

Trophy holders stand a chance to win an unhatched Pixelmon egg and a trainer after the conclusion of the event! \
**The more Proof of Kevin you hold, the higher your chance to win the unhatched egg and trainer!**

## Partnership

Tales of Elleria is poised for its most significant collaboration to date. Should this event turn out to be a hit, Kevin might be back with an even bigger army……

## Join the Adventure

The lands of Elleria are buzzing with excitement. It’s more than just a battle; it’s a fusion of worlds, lore, and communities. We invite every player to be a part of this grand event, to face Kevin and carve their names into Elleria and Pixelmon’s history.

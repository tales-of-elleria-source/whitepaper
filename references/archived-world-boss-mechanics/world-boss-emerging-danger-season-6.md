# World Boss: Emerging Danger (Season 6)

<figure><img src="../../.gitbook/assets/image (11) (1).png" alt=""><figcaption><p>Pachapapa, The Unworldly Invader</p></figcaption></figure>

The brave Ellerian Heroes rallied together with the LifeDAO warriors, forming squadrons to bait out Pachapapa's magic spells, while another charged and attacked, taking turns to flank and deal damage to Pachapapa's shield.

Fresh out of the portal, the magic essence in the air was strong and both the boss and the lifeDAO warriors could use their powers to their full extent, making the start of the fight particularly intense.

After a harsh drawn out battle, the Ellerians-LifeDAO alliance managed to cut through his magic defense and interrupt Pachapapa from weaving his massive spell of destruction!

Morale was high, and everyone got ready for a second attack. Unfortunately, the magic in the air was dissipating, and while the mage grew weaker, it meant that the LifeDAO support troops had to go too.

The Ellerians strategically regrouped and bided their time, continuing to farm medals, goblins, and growing stronger. And now, it's time to push back against Pachapapa!

Due to his magic spells, we need all hands on deck and every hero's participation counts!

Challenge Pachapapa and get him out of our world! Only goldies are welcome!!

## Entrance Ticket

![](<../../.gitbook/assets/image (20) (1).png>)

To enter the boss dungeon, you must have one entrance ticket. These tickets can be crafted from item drops attained from quests or purchased from the adventurer's shop.

\
\
Please refer to the [recipe list](../../gameplay-prologue/crafting-system/crafting.md#consumable-recipes) for the a full recipe list. The recipe for crafting entry ticket is as shown:

<figure><img src="../../.gitbook/assets/image (5) (1) (2).png" alt=""><figcaption><p>Buy tickets from the shop in Adventurer's Guild! Please dont leave the page until the transaction completes.</p></figcaption></figure>

<figure><img src="../../.gitbook/assets/image (2) (2).png" alt=""><figcaption><p>Craft tickets for a cheaper entry!</p></figcaption></figure>

## World Boss Structure

This season, players will be ranked according to the **highest cumulative damage done across all attempts for their best hero.**

E.G. You have three heroes:\
\- Hero 1 does 10,000 damage across 5 attempts\
\- Hero 2 does 2,000 damage across 3 attempts\
\- Hero 3 does 5,000 damage across 1 attempt

Your rank on the Leaderboard will be 10,000 damage done (Hero 1 has the most damage).

\
**The hero with the highest damage irregardless of attempt will be ranked on the Leaderboard.**

## World Boss Mechanics

* There'll be a set pattern for the first ten turns but subsequently randomized between normal & special attack at a specific chance.
* Pachapapa's normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Pachapapa will be. Beyond ten turns, Pachapapa enters into rage mode and will have progressively increasing stats.
* You can use one potion per turn, same as in normal quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated. The leaderboard however will update every 15 minutes.
* Pachapapa stats will remain the same throughout the event period.

<table><thead><tr><th width="233.05650952232276">Stats</th><th>Amount</th></tr></thead><tbody><tr><td>Intelligence</td><td>80</td></tr><tr><td>Agility</td><td>40</td></tr><tr><td>Endurance</td><td>70</td></tr><tr><td>Will</td><td>40</td></tr></tbody></table>

## $MAGIC Rewards

Distributed $MAGIC will come from entry tickets and the Treasure emission grants (TIP 09).

### Reward pool: TIP09 $MAGIC + Percentage of Entrance Fees

<figure><img src="../../.gitbook/assets/image (3) (2) (2).png" alt=""><figcaption><p>You can see $MAGIC pool, time left, distribution, damage, from the leaderboards.</p></figcaption></figure>

This season, the starting reward pool is increased to 8,400 $MAGIC.\
\
$MAGIC Distribution: Allocated across the damage Leaderboard

**1st Place**: 5% of the pool\
**2nd Place**: 4% of the pool\
**3rd Place**: 3% of the pool\
**4-10th Place**: 2% of the pool each\
**Top 25 Place**: 1.5% of the pool each\
**Top 50 Place**: 1% of the pool each\
**Top 100 Place**: 0.3% of the pool each\
**Top 200 Place**: 0.1% of the pool each\
**Special**: The player with the **most attempts** gets **1.5%** of the pool\
**Special**: Players that achieve a rank containing “6” in its digit get an additional **10,000 MEDALS Participation Reward**: **3,000 MEDALS**\
\
Do at least 1 damage to count towards the reward pool!

Rewards will be distributed after the event is over to all players at once. Players will not receive any rewards immediately from the quest.

---
description: ⚠️ The ground trembles and a wild woodot appears! ⚠️
---

# World Boss: Spellborne Woodot (Season 11/12)

F_rom the enchanted realms of Spellborne, the curious Woodot has emerged in Elleria’s forests. This creature, distinguished by a leaf on its head, has disturbed the ancient magic of the woods. Heroes, it’s time for action! Arm yourselves and unite for a thrilling encounter with Woodot. Ready yourselves for a clash that blends mystery with bravery. The adventure begins now!_

<figure><img src="../../.gitbook/assets/image (67).png" alt=""><figcaption></figcaption></figure>

{% hint style="success" %}
Up to 50,000 USD in prizes up for grabs! Read to find out how you can participate and make the most out of this newcomer-friendly limited event!
{% endhint %}

{% hint style="success" %}
**New to Tales of Elleria?** Holding a **Spellborne Apartment** awards you a [generous amount of resources](world-boss-season-11.md#spellborne-collaboration-perks) to let you excel on the leaderboard (snapshot already taken)! Just login with the same wallet into https://app.talesofelleria.com/ to claim your rewards from the in-game mail!
{% endhint %}

{% hint style="success" %}
There are 2 separate leaderboards, one especially for new players. From 18th March 2024 to 25th March 2024, only Ethereal Heroes (free heroes) can participate, and these heroes will automatically be boosted to LEVEL 30 with MAX STATS!\
\
Read on for more information! \~25,000 USD available in prizes from this leaderboard!
{% endhint %}

### Cross-Ecosystem Collaboration <a href="#cross-ecosystem-collaboration" id="cross-ecosystem-collaboration"></a>

Elleria will always welcome collaborations that amplify the gaming experience and knit virtual worlds closer. We're proud to unveil Woodot from [Spellborne](https://twitter.com/spellbornegame) as the next world boss!

<figure><img src="../../.gitbook/assets/image (68).png" alt=""><figcaption></figcaption></figure>

The introduction of _Woodot_, a lovable character from Spellborne, into Tales of Elleria, accompanies our narrative of cross-IP development to expose players to different ecosystems. Come challenge Woodot and show him the power of the Ellerians!

## Event Info

<figure><img src="../../.gitbook/assets/image (69).png" alt=""><figcaption><p>Head over to the City Gates > World Boss to find the Woodot Quest!</p></figcaption></figure>

Dive into the heart of Tales of Elleria for this special World Boss event, where you can lead your ToE heroes into epic battles. You can access the event via [https://app.talesofelleria.com/](https://app.talesofelleria.com/) with a compatible web3 wallet.

***

There are two event periods with different entry criteria:

**EVENT PERIOD 1 - Ethereal Only**

**Duration**: 18th March 2024 to 25th March 2024 (4AM UTC).\
**Rewards**: 12,500 $ARB distributed across 1000 players.\
**Entry Criteria**:  \
\- 1 World Boss Ticket (purchaseable from Shop or Crafted, see below).\
\- Ethereal Heroes only (free heroes!)\
\- 1 Quest Attempt per entry (blue bar indicated on the hero card, resets 4AM UTC)\
**Special Mechanics:** \
\- All entry participants will be automatically boosted to min. Level 30 and have MAX STATS. Any participants above level 30 will remain at their levels.\
\
**Scoring:**\
\- Highest combined damage dealt across all heroes and all attempts.

{% hint style="info" %}
With the special mechanic, everyone starts on a level playing field. To stand out:\
\- Craft and use equipment and consumables.\
\- Purchase more ethereal heroes from the Summoning Altar.\
\- Send in entries consistently!
{% endhint %}

&#x20;**EVENT PERIOD 2 - Open Category**

**Duration**: 25th March 2024 to 1st April 2024 (4AM UTC).\
**Rewards**: 12,500 $ARB distributed across 1000 players.\
**Entry Criteria**:  \
\- 1 World Boss Ticket (purchaseable from Shop or Crafted, see below).\
\- 1 Quest Attempt per entry (blue bar indicated on the hero card, resets 4AM UTC)

**Scoring:**\
\- Highest combined damage dealt across all heroes and all attempts.

{% hint style="info" %}
In this season, the strongest Genesis/Dawn heroes stand out. \
Emerge champion from this free-for-all! :crown:
{% endhint %}

***

To learn more about obtaining a hero and starting, please refer to our [quickstart guide](https://docs.talesofelleria.com/welcome-to-elleria/guides/general-game-guide) or ask in our Discord! An overview is also available in the next section.

## Dive Deep into the World Boss Structure <a href="#dive-deep-into-the-world-boss-structure" id="dive-deep-into-the-world-boss-structure"></a>

#### **Step 1: Obtaining a Hero** <a href="#step-1-obtaining-a-hero" id="step-1-obtaining-a-hero"></a>

![](https://docs.talesofelleria.com/\~gitbook/image?url=https:%2F%2Fcdn-images-1.medium.com%2Fmax%2F800%2F1\*XPM9Ivfuh-O9wvpexulvtw.png\&width=768\&dpr=4\&quality=100\&sign=270c888751e915a880125c557dc1271c71a2b8153e43d9b9d79576cebf2a3791)

Before you can challenge Woodot, you’ll need a hero by your side. For newcomers in Tales of Elleria, you can obtain a [free hero](https://medium.com/@talesofelleria/ethereal-heroes-a-new-era-5eef5b2b9c82) through the summoning altar, or purchase a tradable hero from the [trove marketplace](https://app.treasure.lol/collection/tales-of-elleria). Assemble your team and prepare them for the grand battle that awaits!

#### **Step 2: Securing Entry Tickets** <a href="#step-2-securing-entry-tickets" id="step-2-securing-entry-tickets"></a>

![](https://docs.talesofelleria.com/\~gitbook/image?url=https:%2F%2Fcdn-images-1.medium.com%2Fmax%2F800%2F0\*glo9govE5sO1jvAE\&width=768\&dpr=4\&quality=100\&sign=ae25bf5616d85c0dd18f00ca92f95ad16e6ee921f1a4e0b511b93c5fe510cebb)

You will require an entry ticket for every challenge attempt against Woodot. Hoard these tickets by crafting them using quest item drops, purchasing from [Trove Marketplace](https://app.treasure.lol/collection/tales-of-elleria-relics/20000), or from the In-Game Adventurer's Guild Shop.

![](https://docs.talesofelleria.com/\~gitbook/image?url=https:%2F%2F269266869-files.gitbook.io%2F%7E%2Ffiles%2Fv0%2Fb%2Fgitbook-x-prod.appspot.com%2Fo%2Fspaces%252FRkCojz7b5iZw9GcV2pjT%252Fuploads%252Fgit-blob-70156f5db80ace722b8702d1cf6440f52c0616b7%252Fimage.png%3Falt=media\&width=768\&dpr=4\&quality=100\&sign=7720bf1fe012cef702d6eb95cf67f32ebd0acb6b66824c6bb7ca48f9401a26be)

> Hint: It is cheaper to [craft your entry tickets](https://docs.talesofelleria.com/gameplay-prologue/crafting-system/crafting#consumable-recipes) than purchase them from Trove marketplace or from the adventurer’s shop!

#### **Step 3: Engage in Battle** <a href="#step-3-engage-in-battle" id="step-3-engage-in-battle"></a>

In this boss fight, heroes will attempt to deal as much damage as possible before running out of HP. The **total amount of damage done toward the Woodot across all heroes and attempts in a wallet** will be ranked on the leaderboard where prizes will be allocated.

A player can send an unlimited number of heroes into the world boss if he has sufficient entry tickets and quest attempts to rack up more points.

{% hint style="warning" %}
The leaderboard snapshot happens immediately before rewards distribution. Your hero must remain in your wallet until rewards are distributed or points may be lost.\
\
You won't be affected if you don't move your heroes until after you claim your reward!
{% endhint %}

### **Battle Mechanics**

* The battle utilizes a turn-based system, and the boss can either use a special/normal attack per turn. Special attacks will hit for a minimum of 15% of the hero's HP.&#x20;
* Woodot will get empowered the longer the battle drags on. Beyond the 10th turn, Woodot will have progressively increasing stats.
* You can use one item per turn.
* A quest must be completed before the final cut-off time for its damage to count.
* A hero must remain in your wallet for its score to count. If the hero quests on a different account, all previous quest attempts are voided. (If the hero moves from A > B > A, all attempts from the original A before transfer are also voided)
* The Woodot does Magical damage and will have fixed Stats throughout the event.

<figure><img src="../../.gitbook/assets/image (70).png" alt=""><figcaption></figcaption></figure>

## Woodot World Boss Rewards <a href="#rewards-await-the-brave" id="rewards-await-the-brave"></a>

In each event period, there will be 12,500 $ARB allocated up to the 1,000th player, for a total of 25,000 $ARB rewards from the Arbitrum Short-Term Incentive Program.

The **$ARB prize pool allocation per event period** is as follows:

1st Place ► 500 $ARB\
2nd Place ► 375 $ARB\
3rd Place ► 250 $ARB\
4–10 Place ► 150 $ARB\
11–25th Place ► 100 $ARB\
26–50th Place ► 75 $ARB\
51–100th Place ► 60 $ARB\
101–200th Place ► 12.5 $ARB\
201–600th Place ►5 $ARB\
601–1000th Place ► 1.75 $ARB

<figure><img src="https://miro.medium.com/v2/resize:fit:96/1*UMcb25GzgXfAoPwoq4Js6w.png" alt="" height="74" width="96"><figcaption><p>Spellborne Classic Chest</p></figcaption></figure>

In addition, Spellborne has also generously contributed to the **Open Category Prize Pool**:

1st Place ► 1x [Spellborne Apartment](https://opensea.io/collection/spellborne-apartments)\
2nd Place ► 1x [Spellborne Apartment](https://opensea.io/collection/spellborne-apartments)\
3rd Place ► 1x [Spellborne Apartment](https://opensea.io/collection/spellborne-apartments)\
Top 100 ►1x Classic Chest

This chest contains guaranteed rewards such as 3x Medium Health Packs, 3x Medium Stamina Packs, 2x Revives, and 5x Random Seedpacks to prepare you for your next adventures in Spellborne!

### Proof of Participation <a href="#usdarb-rewards" id="usdarb-rewards"></a>

<figure><img src="../../.gitbook/assets/image (71).png" alt="" width="360"><figcaption><p>Woodot Trophy!</p></figcaption></figure>

For every entry into the Woodot World Boss, receive a soul-bound Woodot Trophy!

## Spellborne Collaboration Perks

### Spellborne Apartment Holders' Exclusive Rewards

<figure><img src="../../.gitbook/assets/image (72).png" alt=""><figcaption><p>FREE REWARDSS!</p></figcaption></figure>

Owners of Spellborne Apartment NFTs will receive a special boon per wallet — **1x World Boss Ticket, 50,000 $MEDALS, 3x Timeless Memories (300 Hero EXP), and a full Rookie Equipment Set,** boosting your chances in the upcoming grand battle against Woodot. A snapshot was taken on 15th March 2024. Check your mail in your Elleria Account to claim your rewards, including a free entry into Woodot!

> _This mail expires on 1st April 2024 if unclaimed. Make sure to claim it!_

### Woodot Login Daily Gifts!

<figure><img src="../../.gitbook/assets/image (73).png" alt=""><figcaption></figcaption></figure>

To bolster our defenses, all new and old players can participate in our 7-day campaign system running during the World Boss event.

**Sign in each day to receive the following rewards**\
Day 1: 5000 $MEDALS\
Day 2: 1x T2 Guards (SB)\
Day 3: 5000 $MEDALS\
Day 4: 1x T2 Charm (SB)\
Day 5: 5000 $MEDALS\
Day 6: 1x T2 Circlet (SB)\
Day 7: 1x Lesser Positive Scroll (SB)

Don’t worry if you can’t sign in every day! You can still pick up where you left off and claim the next reward when you’re back. The event runs for 14 days, and players can log in and claim on any 7 days within this period to collect all their rewards! (Note: The bonus will not progress if the day’s rewards are not claimed!)

## FAQ

{% hint style="info" %}
Special Attacks' damage can be reduced by Skill DEF % Infusions, but it will not be reduced below 15% HP.
{% endhint %}

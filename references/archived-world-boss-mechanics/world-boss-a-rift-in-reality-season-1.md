# World Boss: A Rift in Reality (Season 1)

![Zorag, the Goblin King](<../../.gitbook/assets/goblin\_camp\_ex\_withking (2).png>)

A great rift tore through the sky, enveloping the City of Elleria. Through this rift came a huge monster that started wreaking havoc all over the city.

Challenge Zorag and save Elleria!

## Entrance Ticket

![](<../../.gitbook/assets/image (20) (1).png>)

To enter the boss dungeon, you must have one entrance ticket. These tickets can be crafted from item drops attained from quests or purchased from the adventurer's shop.\
\
Please refer to the [recipe list](../../gameplay-prologue/crafting-system/crafting.md#consumable-recipes) for the recipe to craft this ticket.

## World Boss Structure

In this boss fight, heroes will attempt to deal as much damage as possible before perishing to the world boss. Rewards will be allocated according to ranking.

A player can send an unlimited number of heroes into the world boss so long as he has sufficient entry tickets. Please note that quest attempts are shared with normal quests in the Goblin Plains. The total cumulative damage done from all heroes by each player will be ranked on the leader board.

## World Boss Mechanics

* There'll be a set pattern for the first ten turns but subsequently randomized between normal & special attack at a specific chance.
* Zorag's normal attack will have a minimum 25% chance of hitting a player, while his special attacks have increased accuracy.
* The longer the battle, the more invigorated Zorag will be. Beyond ten turns, Zorag enters into rage mode and will have progressively increasing stats.
* You can use one potion per turn, the same as in normal quests.
* Any damage immediately adds towards the total damage dealt, and you do not need to wait for the hero to be defeated.
* Zorag's stats will remain the same and will not be randomized

<table><thead><tr><th width="233.05650952232276">Stats</th><th>Amount</th></tr></thead><tbody><tr><td>Strength</td><td>60</td></tr><tr><td>Agility</td><td>40</td></tr><tr><td>Endurance</td><td>50</td></tr><tr><td>Will</td><td>30</td></tr></tbody></table>

## $MAGIC Rewards

Distributed $MAGIC will come from entry tickets and the Treasure emission grants (TIP 09).

### Reward pool: TIP09 $MAGIC + Percentage of Entrance Fees

$MAGIC Distribution: Allocated across the damage leaderboard

![World Boss Rankings](<../../.gitbook/assets/image (17) (2).png>)

**1st Place**: 12% of the pool + Commemorative NFT reward\
**2nd Place**: 8% of the pool\
**3rd Place**: 6% of the pool\
**4-10th Place**: 2% of the pool (14%)\
**Top 50 Place**: 0.5% each (20%)\
**Top 100 Place**: 0.4% each (20%)\
**Top 200 Place**: 0.2% each (20%)\
Participation reward: One random relic awarded after bi-weekly first clear.\
Do at least 1 damage to count towards the reward pool!

Rewards will be distributed after the event is over to all players at once. Players will not receive any rewards immediately from the quest.

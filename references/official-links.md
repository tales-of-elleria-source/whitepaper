---
description: 'Official Game Site: https://app.talesofelleria.com'
---

# Official Links

{% hint style="success" %}
Official Game Site: [https://app.talesofelleria.com](https://app.talesofelleria.com)
{% endhint %}

| Socials/Info                  | Link                                                                                                                                             |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| Twitter                       | [https://twitter.com/talesofelleria](https://twitter.com/talesofelleria)                                                                         |
| Discord                       | [https://discord.gg/xmt7FaEZfY](https://discord.gg/xmt7FaEZfY)                                                                                   |
| Medium                        | [https://medium.com/@talesofelleria](https://medium.com/@talesofelleria)                                                                         |
| Landing Page                  | [https://www.talesofelleria.com](https://www.talesofelleria.com)                                                                                 |
| Whitepaper (Change Proposals) | [https://gitlab.com/tales-of-elleria-source/whitepaper/-/issues](https://gitlab.com/tales-of-elleria-source/whitepaper/-/issues)                 |
| Smart Contract Source Codes   | [https://gitlab.com/tales-of-elleria-source/contracts/elleria-contracts](https://gitlab.com/tales-of-elleria-source/contracts/elleria-contracts) |

{% hint style="info" %}
For transparency, security, and to encourage ecosystem growth, our contracts are all verified on arbiscan.io and their sources are available as above.
{% endhint %}

| Marketplace       | Link                                                                                                                                          |
| ----------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| Trove - Heroes    | [https://trove.treasure.lol/collection/tales-of-elleria](https://trove.treasure.lol/collection/tales-of-elleria?tab=storefront)               |
| Trove - Relics    | [https://trove.treasure.lol/collection/tales-of-elleria-relics](https://trove.treasure.lol/collection/tales-of-elleria-relics?tab=storefront) |
| Trove - Equipment | [https://trove.treasure.lol/collection/tales-of-elleria-equipment](https://trove.treasure.lol/collection/tales-of-elleria-equipment)          |

### Submitting a Whitepaper Change Proposal (using Gitlab)

1. Go to the Whitepaper's Gitlab page: [https://gitlab.com/tales-of-elleria-source/whitepaper/-/issues](https://gitlab.com/tales-of-elleria-source/whitepaper/-/issues) (and sign in/register to Gitlab if you haven't)
2.  Click on "New Issue"

    <figure><img src="../.gitbook/assets/image (55).png" alt=""><figcaption></figcaption></figure>
3. Enter a Title and Description. You can copy the following template for the description:

```
### Relevant Page(s):
https://app.gitbook.com/s/RkCojz7b5iZw9GcV2pjT/~/changes/7/references/official-links

### Original Content:
Insert content here.

### Proposed Changes/Missing Content to Add/Outdated Content/...:
Insert proposed changes here.

### Notes:
...
```

4. Click 'Create Issue', and done! Ping us in Discord if you want us to take a look more quickly!

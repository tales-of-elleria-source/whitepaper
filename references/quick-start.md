---
description: A checklist & summarized way to help you get started or continue your journey.
---

# Quick Start

### Welcome to Tales of Elleria

Refer to [this link](https://discord.com/channels/933280677338701826/933280677879762986/979475791337381970) for a full visual guide on how to complete the core gameplay loop.

![](<../.gitbook/assets/image (13) (1).png>)

**This article serves as an overarching guide on how to play the game. For more information, reach out to anyone in Discord, read through the Whitepaper, and explore-**

**We hope you enjoy your stay!**

\----------------------------------------------------------

### **To Get Started:**

**Understand the core gameplay loop:**

1. Mint Heroes ([https://medium.com/@talesofelleria/joining-tales-of-elleria-2023-60d73371e841](https://medium.com/@talesofelleria/joining-tales-of-elleria-2023-60d73371e841))
2. Send Heroes on Assignments for $MEDALS.
3. Use $MEDALS to send heroes on Quests for $ELM
4. More $ELM = :thumbsup:
5. Rinse and Repeat

**You will need heroes to accompany you:**

1. Summon (Mint) heroes from [the game](https://app.talesofelleria.com/), through the Summoning Altar.\
   Genesis Mint is currently ongoing, at 0.05 ETH per hero.
2. Buy heroes from[ the marketplace](https://trove.treasure.lol/collection/tales-of-elleria?tab=related).

**Understanding Bridging:**

1. Heroes/Relics must all be bridged into the game to use them. You can do so from Summoning Altar > Manage Souls (Heroes) / Manage Relics.
2. Bridged into game = Gasless transactions = :thumbsup:
3. You will only need to release your heroes when you want to sell them on the marketplace.

**Understand your heroes:**

* **CS HEROES ARE VERY VALUABLE**: Understand what a 'CS' is: Class specific (CS) means your hero has >= 86 main stat and >= 61 secondary stat, **which make them eligible for 'better assignments'**. You can take reference to the image for easier visualization.

![](<../.gitbook/assets/image (12) (2).png>)

* Understand how to strengthen your heroes:
  * Levelling increases combat stats for quests.
  * Scrolls alter your base stats for assignments + quests.

**Understand Assignments:**

* Assignments = Passive $MEDALS. Claim once every 7 days, or there is an[ emissions decay](../gameplay-prologue/game-features/assignments/).
* Level 2 CS heroes can go on Class-Specific (CS) assignments for huge $MEDALS. You might hence want to prioritize leveling CS heroes, and get one as your main $MEDALS farmer.

**Understand Quests:**

* Every hero can attempt quests daily. Number of attempts depends on your level. Quest requires $MEDALS from assignments.
* Different quests = different risk/rewards ratio. For $MEDALS, go on 'Goblin Plains - Adventure Begins', for $ELM, go on harder quests.
* There is a 'best strategy' for which quest you should send your heroes on, depending on their stats + level, that will yield the best rewards. Research & find a [best strategy](../gameplay-prologue/core-gameplay-loop/player-types.md) for yourself!

{% hint style="info" %}
Level 1 Common Heroes should stick to 'Goblin Plains - Adventure Begins'. This quest yields an average of +25 $MEDALS and will also drop relics, which are important in the crafting gameplay loop.
{% endhint %}

\---

### Going Deeper:

* **Mint Heroes ->**[ (Guide available here)](https://medium.com/@talesofelleria/joining-tales-of-elleria-2023-60d73371e841) Stand a chance to obtain CS and Questoors at entry level costs!
* **Understand stats further** -> combat attributes affect your quests & which heroes are worth more/less depending on their stats distribution: [attributes](../gameplay-prologue/heroes/attributes/ "mention")
* **Crafting** -> Understand which relics are important for certain recipes, and hoard them [crafting-system](../gameplay-prologue/crafting-system/ "mention")
* **Quest Optimization** -> You can go on quests right before the daily reset to reduce the downtime from the 30 minute assignments no-reward window.
* **Special Attacks/Defend** -> Read up on what these do and determine if they are worth using for you[class-skills-traits.md](../gameplay-prologue/heroes/class-skills-traits.md "mention")
* **Self-Help Functions** -> Heroes desynced or Relics missing? Try these: ![](<../.gitbook/assets/image (10) (1).png>)
* **Jesters & Plague Doctors** -> What are these characters that have special art in the marketplace with a red border in-game? [Hmmm....](https://discord.com/channels/933280677338701826/933280677879762986/960202113609248799)
* **Boosts** -> Read up on the previous/on-going boosts events to find out how to make the best of it or to prepare for future events.
* **Do Daily Tasks** -> [Here!](../gameplay-prologue/game-features/tasks.md)
* **Follow our #announcements, #game-updates, and Twitter to receive the latest news!**
* **Socialize and join us on Discord!** Share strategies and join our awesome community :smile:
* **Share your battle and mint results!**
* **Shill Tales of Elleria to your friends!** Word of mouth is the best way to build a strong community and project.
* **Understanding MagicSwap** -> how to get $ELM without needing to play the game
* **Liquidity Provision** -> Want other ways to get stat boosts? Stake LP tokens at the bank

**Fortuna Eterna**

---
description: >-
  The official documentation and whitepaper for Tales of Elleria. If any content
  is missing or seems outdated, do reach out in Discord for an update!
---

# Tales of Elleria

![Welcome Ellerians, ignite your adventerous spirit and get ready for an epic story to come.](.gitbook/assets/Toe\_TwitterBanner-01.png)

## Introduction

Tales of Elleria (TELL) is a GameFi project on Arbitrum and is the entry point into Elleria's metaverse.

Elleria utilizes several tokens and NFTs to create our internal economy and an RPG-like progression system.

* **Heroes** - NFT (base characters with progression system)
* **Equipment** - NFT (character progression)
* **Drops/Relics** - NFT (crafting/boosts/consumables/resources)
* **$ELLERIUM** (ELM) - Main Token
* **$MEDALS** (MEDALS) - Supplementary Token

Heroes are essential to your adventure. They can be trained, upgraded, sent on assignments, and go on quests to obtain Tokens, Equipment, Drops, and Relics.

$ELM and $MEDALS are required resources for your Heroes to perform activities in-game to obtain rewards and further your progression.

The dependency between Tokens and NFTs creates an internal circular economy that balances production and consumption, aiming to maximize the values of both to create a long-term sustainable project.

The further your progression, the more rewarding (and riskier) your journey!

Who will you choose to be?

> A _humble protector_, satisfied with clearing the town's outskirts, in lesser danger with stable returns.
>
> A _thrill-seeker_, venturing out into the unknown, not knowing the dangers, greedy for the rewards.
>
> A _true adventurer_, well-informed and equipped, attempting increasingly difficult quests while mitigating risks.

Players will unlock content and lore as a collective when they clear quests. Discover the truth behind the Cataclysm, the origins of the Heroes and Monsters, the story of Elleria.

The world will progress according to the community's actions.\
**Write your tale, and bards shall sing of it.**

{% embed url="https://app.talesofelleria.com" %}
Start playing now!
{% endembed %}

## Vision & Goals

TELL aims to be a decentralized GameFi project that provides an immersive & fun gaming experience packaged together with DeFi elements.

With the advent of Blockchain technology, our team believes that games and metaverses can finally be community-written and community-owned. TELL will start as a singular continent, poised to take root within the Blockchain space and expand with the community.

We hope to serve as a role model for decentralization and transparency across the GameFi ecosystem. Our contracts and endpoints will be made public to encourage and facilitate the creation of community-owned projects. While the founding team has a roadmap and general direction, the community can influence these at any time with proposals.

Elleria also aspires to create a hub that serves as a bridge across different projects. Currently, assets from various projects stay isolated despite our contracts sharing the same standards. We aspire to integrate assets from other projects to elevate Blockchain immersion and create an integrated community, especially within our 3D open-world (see Phase Beta).

{% hint style="info" %}
**Did you know:** The project was initially developed for BSC but switched to Arbitrum after a community vote.
{% endhint %}

![Voters consisted of OG discord members that found our project on Twitter.](<.gitbook/assets/arbitrum (1).jpg>)

## Inspiration & Prospects

The inspiration for the GameFi side of the project came from off-chain RPG games such as Final Fantasy, Genshin Impact, Maplestory, amongst many others. We're designing the initial mechanics around an RPG system integrated with Blockchain technology. In terms of world-building, monster tropes, the overall atmosphere, and art style, we have created a crossover using JRPG and Westernized elements that can allow for cross-platform integration in the future.

The Ellerian metaverse will be community-driven. As TELL is the spearhead of this metaverse, our founding team will be in charge of the initial development and release for Tales of Elleria. However, our long-term vision involves the community, and we welcome everybody's involvement in our world's expansion.

As Elleria continues to develop, assets within our metaverse will become increasingly valuable with the consistent release of new content and depth. We believe that continued development propelled by community governance, contribution, and involvement, will attract collaborators and spur the creation of publicly hosted projects, that further raises and sustains the value of the Ellerian Metaverse.

Cheers to everyone in our community, our moderators, collaborators, partners, and team! None of this would be possible without all of you. May Elysis light our way towards our brightest futures.

![Cheers to all adventurers!](.gitbook/assets/guild\_teaser.jpg)

{% hint style="info" %}
Want to help us improve the Whitepaper? Suggest changes here: [#submitting-a-whitepaper-change-proposal](references/official-links.md#submitting-a-whitepaper-change-proposal "mention")
{% endhint %}

